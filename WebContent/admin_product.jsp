<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>MGBD Parts ADMIN</title>
    <link rel="shortcut icon" href="icono/favicon.ico">
    <link rel="stylesheet" type="text/css" href="AdminComple/navbarAdmin.css">
    <link rel="stylesheet" type="text/css" href="AdminComple/admin_Products.css">
    <script type="text/javascript" src="Recursos/jquery.js"></script>
    <script type="text/javascript" src="AdminComple/adminProfile.js"></script>  

</head>
<body>
<jsp:include page="AdminComple/navbarAdmin.jsp"></jsp:include>
		<article class="Contenido">
            <h2 class="Title">Products</h2>
            <h3 id="AddProducts">Add Products</h3>
            <form id="AddProduct"class="Add_Product"  action="ProductsAdminServlet" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="AddProduct">
                <label>Product Name
                    <input id="Add_Name"type="text" name="ProductName">
                </label>
                <label>Description for the Product
                    <textarea  id="Add_Desc" name="Descript"></textarea>
                </label>
                <label>Quantity Available
                    <input id="Add_Quant" style="width:50%;" type="text" name="QuanAvailable" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                </label>
                 <label>Price
                    <input id="Add_Price" style="width:50%;" type="text" name="Price" onkeypress='return event.charCode >= 48 && event.charCode <= 57 || event.charCode==46 '>
                </label>
                <label>Category
                	<Select style="width:100%;backgound:black;font-size:1rem;" name="Category">
                		<c:forEach var="tempCate" items="${ListaCatego}">
                			<c:if test="${tempCate.isEstado()==true }">
                				<option value="${tempCate.getID()}">${tempCate.getCatego()}</option>
                			</c:if>
                		</c:forEach>
                	</Select>
                </label>
                  <label>Part
                	<Select style="width:100%;backgound:black;font-size:1rem;" name="Part">
                	<c:forEach  var="tempParts" items="${ListaParts}">
                		<c:if test="${tempParts.getEstado()>0}">
                			<option value="${tempParts.getID_Parts()}">${tempParts.getPart()}</option>
                		</c:if>
                	</c:forEach>               		
                	</Select>
                </label>
                <label style="padding:0;width:12%;margin:0 auto; display: inline-block;">Active</label>
                <label class="switch" style="margin:0 auto">                    
                    <input type="checkbox" checked="checked" name="Active">
                    <span class="slider"></span>
                </label>
                <label style="padding:0;width:12%;margin:0 auto; display: inline-block;">Publish</label>
                <label class="switch" style="margin:0 auto">                    
                    <input type="checkbox" checked="checked" name="Publish">
                    <span class="slider"></span>
                </label>
                <label>Choose an Image and/or Video for the product
                    <input id="Add_Image" type="file" name="Product_Image" multiple>
                </label>
                
                <input type="hidden" name="Form" value="AddProduct">
                <input id="Add_Submit"type="submit" value="Add Product">
                <div id="Add_Info"></div>
            </form>
            <article id="EditProducts" class="Edit_Product">
                <h3 id="AddProducts">Edit Products</h3>
                <article class="Search_Product">
                    <header>
                        <form class="Searcher" method="POST">
                            <input type="text" placeholder="Search a Product">
                            <input type="submit" value="Search">
                        </form>
                    </header>
                    <section class="Results">   
                        <div class="table_Container" role="table">
                            <div class="table_header" role="rowgroup">
                                <div class="table_rows" role="columnheader"><label>Name</label></div>
                                <div class="table_rows" role="columnheader"><label>Category</label></div>
                                <div class="table_rows" role="columnheader"><label>Part</label></div>
                                <div class="table_rows" role="columnheader"><label>Rating</label></div>
                                <div class="table_rows" role="columnheader"><label>Units</label></div>
                            </div>
                            <c:forEach var="tempProducts" items="${ListaProduct}">
                            	<div class="table_content" role="rowgroup">
	                               <div class="table_rows wdata" role="cell"><a href="ProductsAdminServlet?IDProduct=${tempProducts.getID()}">${tempProducts.getArticulo()}</a></div>
	                               <div class="table_rows wdata" role="cell"><a href="ProductsAdminServlet?IDProduct=${tempProducts.getID()}">${tempProducts.getsCategoria()}</a></div>
	                               <div class="table_rows wdata" role="cell"><a href="ProductsAdminServlet?IDProduct=${tempProducts.getID()}">${tempProducts.getsPart()}</a></div>
	                               <div class="table_rows wdata" role="cell"><a href="ProductsAdminServlet?IDProduct=${tempProducts.getID()}">${tempProducts.getRating()}</a></div>
	                               <div class="table_rows wdata" role="cell"><a href="ProductsAdminServlet?IDProduct=${tempProducts.getID()}">${tempProducts.getUnidad()}</a></div>
                            	</div>
                            </c:forEach>
                            
                        </div>
                    </section>
                </article>
                <form id="Form_EditProduct" style="margin-bottom:5em;"class="Add_Product"  action="ProductsAdminServlet" method="POST" enctype="multipart/form-data">
                    
                    <label>Product Name
                        <input id="Edit_Name" type="text" name="ProductName" value="${productoGET.getArticulo()}">
                    </label>
                    <label>Description for the Product
                        <textarea  id="Edit_Desc" name="Descript">${productoGET.getDescripcion()}</textarea>
                    </label>
                    <label>Quantity Available
                        <input id="Edit_Quant" type="text" name="QuanAvailable" value="${productoGET.getUnidad()}" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                    </label>
                     <label>Price
                    	<input id="Edit_Price" style="width:50%;" type="text" name="Price" value="${productoGET.getPrecio()}" onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 110'>
                	</label>
                    <label>Category
                	<Select style="width:100%;backgound:black;font-size:1rem;" name="Category">
                	<c:forEach var="tempCate" items="${ListaCatego}">
                			<c:if test="${tempCate.isEstado()==true }">
                				<c:choose>
                					<c:when test="${productoGET.getiCategoria()==tempCate.getID()}">
                						<option value="${tempCate.getID()}" selected="selected">${tempCate.getCatego()}</option>
                					</c:when>
                					<c:otherwise>
                						<option value="${tempCate.getID()}">${tempCate.getCatego()}</option>
                					</c:otherwise>
                				</c:choose>
                			</c:if>
                		</c:forEach>
                	</Select>
                	</label>
                 	 <label>Part
                	<Select style="width:100%;backgound:black;font-size:1rem;" name="Part">
                		<c:forEach  var="tempParts" items="${ListaParts}">
                		<c:if test="${tempParts.getEstado()>0}">
                			<c:choose>
                				<c:when test="${productoGET.getiPart()==tempParts.getID_Parts()}">
                					<option value="${tempParts.getID_Parts()}" selected="selected">${tempParts.getPart()}</option>
                				</c:when>
                				<c:otherwise>
                					<option value="${tempParts.getID_Parts()}">${tempParts.getPart()}</option>
                				</c:otherwise>
                			</c:choose>
                		</c:if>
                	</c:forEach>
                	</Select>
               	 	</label>
                    <label style="padding:0;width:12%;margin:0 auto; display: inline-block;">Active</label>
                    <label class="switch" style="margin:0 auto">  
                    <c:choose>
                    	<c:when test="${productoGET.isEstado()==true}">
                    		<input type="checkbox" checked="checked" name="Active" value="Activo">
                    	</c:when>
                    	<c:otherwise>
                    		<input type="checkbox"  name="Active" value="Activo">
                    	</c:otherwise>
                    </c:choose>                  
                    <span class="slider"></span>    
                    </label>
                    <label style="padding:0;width:12%;margin:0 auto; display: inline-block;">Publish</label>
                	<label class="switch" style="margin:0 auto">                    
                    	<c:choose>
                    		<c:when test="${productoGET.isPublicado()==true}">
                    			<input type="checkbox" checked="checked" name="Publish" value="Publicar">
                    		</c:when>
                    		<c:otherwise>
                    			<input type="checkbox" name="Publish" value="Publicar">
                    		</c:otherwise>
                    	</c:choose>
                    	<span class="slider"></span>
                	</label>
                    <label>Choose an Image and/or Video for the product
                        <input id="Edit_Image" id="Product_Image" type="file" name="Product_Image" multiple>
                    </label>
                    <div class="Wrap_Img_Container">
                    <c:forEach var="tempImagen" items="${ImagenesPro}">
                    	<div class="wrapImg">                            
                            <div class="Icon_Container">
                                <a href="ProductsAdminServlet?DeleteImageID=${tempImagen.getID()}&IDProduct=${productoGET.getID()}">
                                    <img src="Recursos/Imagenes/Trash.png">
                                    <label>Delete Image</label>
                                </a>                                
                            </div>                            
                            <img src="${pageContext.request.contextPath}/ShowImage/${tempImagen.getLocalizacion()}">
                        </div>                    	
                    </c:forEach>   
                     <div class="Wrap_Video_Container">
                      <c:forEach var="tempVideo" items="${VideosPro}">
                      	  <div class="Video_Container">
                            <video class="myVideo">
                                <source src="${pageContext.request.contextPath}/ShowImage/${tempVideo.getDireccion()}" type="video/mp4">
                             </video>
                             <a href="ProductsAdminServlet?DeleteVideoID=${tempVideo.getiD()}&IDProduct=${productoGET.getID()}">Delete Video</a>
                        </div>
                       
                      </c:forEach>
                    </div>
                    </div>
                    <input type="hidden" name="Form" value="Edit_Product">
                    <input type="hidden" name="IDHidden" value="${productoGET.getID()}">
                    <input id="Edit_Submit" type="submit" value="Edit Product">
                    <div id="Edit_Info"><c:out value="${Aviso}"></c:out></div>
                </form>
            </article>

        </article>
        </section>
</body>
</html>