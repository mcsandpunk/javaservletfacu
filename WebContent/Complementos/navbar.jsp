
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>	

<%
	String Parts[]={"Rover","MGF","MINI"};

	pageContext.setAttribute("Parts",Parts);

%>
	<nav class="navBar">        
        <div class="menu">
            <ul>
                <div class="logo"></div>
               <!-- <li><a href="#">News</a></li> -->            
                <li><a href="#">Parts</a>
                    <ul class="dropdown">
                    <c:forEach  var="tempParts" items="${ListaParts}">
                    
                    <li><a href="#">${tempParts.getPart()}</a></li>
                    </c:forEach>                                     
                    </ul>
                </li>                
                <!--  <li><a href="#">Services</a></li>-->                
                <li class="Form_Container">
                    <form class="Form_Buscar" method="GET" action="">
                        <input type="text" placeholder="Search...">
                        <button id="Form_Bu_Buscar" type="submit"></button>
                    </form>
                </li>  
                <c:choose>
                	<c:when test="${UserInfo==null}">
                		<li id="Link_SignUp"><button class="Enlace_SignUp" ></button><p>SignUp</p></li>  
                		<li id="Link_Login"><button class="Enlace_Login" ></button><p>LogIn</p></li>
                	</c:when>
                	<c:otherwise>
                		<li id="Link_User">
	                    <div class="Enlace_User">
	                        <img src="GetImageInfo?Dec=2&ID=${UserInfo.getID()}">
	                    </div>
	                    <p><c:out value="${UserInfo.getNickname()}"/></p>
	                    <ul>
	                        <li><a href="ProfileServlet">Profile</a></li>
	                        <li><a href="NavbarServlet?Logout=TRUE">LogOut</a></li>
	                    </ul>
                		</li>
                		<li id="Link_Carro"><button class="Enlace_Carro"></button><span class="Cart_num" id="Cart_Cantidad">${productos}</span><p>Cart</p></li>
                	</c:otherwise>
                </c:choose>
                
                
            </ul>
        </div>
    </nav>
    <section class="Modales">
        <article id="LoginID" class="Login_Modal animado">
            <form id="LoginForm" class="Modal_Class" action="NavbarServlet" method="POST">
            <input type="hidden" name="Form" value="Login">
                <div class="Modal_Form_img">
                    <img src="Recursos/Imagenes/UserDefaultIcon.png" class="Avatar">
                </div>
                <div class="Modal_Form_Contenido">                	
                    <input type="email" placeholder="Email" name="email" required>
                    <input type="password" placeholder="Password" name="psw" required>
                    <button id="Submit_Login" type="submit">Login</button>
                    <p>Keep me logged in</p>
                    <label class="switch">
                        <input type="checkbox" checked="checked" name="remember">
                        <span class="slider"></span>
                    </label>
                    <button type="button" onclick="document.getElementById('LoginID').style.display='none';document.getElementsByClassName('Modales')[0].style.display='none';"class="Cancelar_Modal">Cancel</button>
                </div>
            </form>
            <div id="INFO_DIV_LOGIN" ></div>
        </article>
        <article id="SignUpID" class="SignUp_Modal animado">
            <form id="SignUpForm" class="Modal_Class" action="NavbarServlet" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="Form" value="SignUp">
                <div class="Modal_Form_img">
                    <img src="Recursos/Imagenes/UserDefaultIcon.png" class="Avatar">
                </div>
                <div class="Modal_Form_Contenido">
                    <input id="inEmail" type="email" placeholder="Email" name="email" required>
                    <input id="inNickname" type="text" placeholder ="Nickname" name="nick" required >                    
                    <input id="inPass" type="password" placeholder="Password" name="psw" required>
                    <input id="inPassrep" type="password" placeholder ="Confirm Password" name="repsw" required>
                    <p>Choose an image for your profile(optional)</p> 
                    <input id="Image_SignUp" type="file" name="foto">
                    <button id="Submit_SignUp"type="submit" >Register</button>                        
                    <button type="button" onclick= "document.getElementById('SignUpID').style.display = 'none'; document.getElementsByClassName('Modales')[0].style.display='none';"class="Cancelar_Modal">Cancel</button>
                </div>
            </form>
            <div id="INFO_DIV_SIGNUP" ></div>
        </article>
    </section>
