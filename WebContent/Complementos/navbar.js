$(document).ready(function(){
	
	try{
		$(".btn_Card_AddCart").click(function(e){
			var value=$(this).attr("value");
			var button = $(this);
			//window.open("IndexServlet?varPro="+value,"_self");
			
			 $.ajax({
		        	url:"NavbarServlet",
		        	type:"POST",	        	
		        	data:{varPro:value},
		        	success:function(resp){
		        		console.log("success");
		        		console.log(resp);
		        		
		        		switch(resp){
		        		case "NoUserAdd":
		        			 
		        		        button.children().toggleClass("show");
		        		        var tiempo =setTimeout(function(){
		        		           
		        		            button.children().toggleClass("show");
		        		           clearTimeout(tiempo);
		        		        },3000);
		        			break;
		        		default:
		        			let cartnum = $("#Cart_Cantidad");
		        		
		        			cartnum.text(resp);
		        			break;
		        		}
		        	},
		        	error:function(jqHXR,estado,error){
		        		console.log(estado);
		        		console.log(error);
		        	},
		        	complete:function(jqXHR,estado){
		        		console.log(estado);
		        	},
		        	timeout:30000
		        });
			 e.preventDefault();
		});
	}
	catch(error){
		console.log(error);
	}
			
			
	
    /****************************************************************/
    /******************************MODALES***************************/
    /****************************************************************/
	
	

    var modalSignUp=document.getElementById("SignUpID");
    var modalLogin = document.getElementById("LoginID");
    
    var Modales = document.getElementsByClassName('Modales')[0];

    try{
    	document.getElementById("Link_SignUp").onclick=function(){

            Modales.style.display = "flex";        
            modalLogin.style.display="none";
            modalSignUp.style.display = "flex";

            let v =[{s:0},{s:1}];
            $(v[0]).animate(v[1],{
                duration: 400,
                step: function(val){
                   $(".Modales").css("transform",`scale(${val})`);
                }
            })
        };
        
        document.getElementById("Link_Login").onclick=function(){
            Modales.style.display = "flex";        
            modalLogin.style.display="flex";
            modalSignUp.style.display = "none";

            let v =[{s:0},{s:1}];
            $(v[0]).animate(v[1],{
                duration: 400,
                step: function(val){
                    $(".Modales").css("transform",`scale(${val})`);
                }             
            })        
        };
        
        window.onclick = function(event){
            if(event.target==Modales){                        
                let v =[{s:1},{s:0}];
                $(v[0]).animate(v[1],{
                    duration: 400,
                    step: function(val){
                       $(".Modales").css("transform",`scale(${val})`);
                    },
                    done:function(){
                        document.getElementsByClassName('Modales')[0].style.display="none";
                        modalSignUp.style.display="none";
                        modalLogin.style.display="none";
                    }
                })            
            }
        };
        
        
    }catch(error){
    	console.log("Error al mostrar Modales: "+error);
    }
    
    document.getElementsByClassName("logo")[0].onclick=function(){
    	console.log("Click en logo");
        window.open("IndexServlet","_self");
    };
    

  try{
	  $("#Submit_SignUp").click(function(e){
	        var act = $("#SignUpForm").attr("action");
	        var met = $("#SignUpForm").attr("method");
	        
	        let divInfo = $("#INFO_DIV_SIGNUP");
	        
	        let inPass =$("#inPass");
			let inPassrep=$("#inPassrep");
			let inNickname = $("#inNickname");
			let image=$("#Image_SignUp");
			let inEmail = $("#inEmail");
			
	        var datos = new FormData($("#SignUpForm")[0]);
	        $.ajax({
	        	url:act,
	        	type:met,
	        	contentType:false,
	        	processData:false,
	        	cache:false,
	        	data:datos,
	        	success:function(resp){
	        		console.log("success");
	        		console.log(resp);
	        		switch(resp){
	        		case "NoEmail":
	        			divInfo.css("background","#f7be16");
	        			divInfo.text("The email isn't correct");
	        			
	        			inEmail.css("border","0.02rem solid red");
	        			break;
	        		case "PassNoEqual":
	        			divInfo.css("background","#f7be16");
	        			divInfo.text("Password not match");
	        			
	        			
	        			inPass.css("border","0.002rem solid red");
	        			inPassrep.css("border","0.02rem solid red");
	        			break;
	        		case "noContra":
	        			divInfo.css("background","#f7be16");
	        			divInfo.text("Password not valid,at least 4 characters");
	        			        			
	        			inPass.css("border","0.002rem solid red");
	        			inPassrep.css("border","0.02rem solid red");
	        			
	        			break;
	        		case "noNick":
	        			divInfo.css("background","#a72461");
	        			divInfo.text("Please,insert a nickname");
	        			        			
	        			inNickname.css("border","0.02rem solid red");
	        			break;
	        		case "SameEmail":
	        			divInfo.css("background","#a72461");
	        			divInfo.text("The email already exists");
	        			        			
	        			inEmail.css("border","0.02rem solid red");
	        			break;
	        		case "SameNickname":
	        			divInfo.css("background","#a72461");
	        			divInfo.text("The nickname already exists");
	        			        			
	        			inNickname.css("border","0.02rem solid red");
	        			break;
	        		case "SizeBig":
	        			divInfo.css("background","#a72461");
	        			divInfo.text("Image is too big");        			
	        			
	        			image.css("border","0.02rem solid red");
	        			break;
	        		case "Nice":
	        			//window.open("IndexServlet","_self");
	        			window.location.reload(true);
	        			break;
	        		}
	        	},
	        	error:function(jqHXR,estado,error){
	        		console.log(estado);
	        		console.log(error);
	        	},
	        	complete:function(jqXHR,estado){
	        		console.log(estado);
	        	},
	        	timeout:10000
	        });
	        e.preventDefault();
	        console.log("Se oprimio SignUp");
	    });
  }
  catch(error){
	  console.log("ERROR SUBMIT SIGNUP: "+error);
  }

    try{
    	$("#Submit_Login").click(function(e){
        	var act = $("#LoginForm").attr("action");
            var met = $("#LoginForm").attr("method");
            let divInfo =$("#INFO_DIV_LOGIN");
            $.ajax({
            	url:act,
            	type:met,
            	data:$("#LoginForm").serialize(),
            	success:function(resp){
            		console.log("Respuesta: "+resp);
            		switch(resp){
            		case "UserNotFound":
            			divInfo.css("background","#f7be16");
            			divInfo.text("Incorrect User or Password");
            			break;
            		case "AdminFound":
            			window.open("AdminServlet","_self");
            			break;
            		case "UserFound":
            			//window.open("IndexServlet","_self");
            			window.location.reload(true);
            			break;
            			
            		}
            		console.log("Success");
            	},
            	error:function(jqHXR,estado,error){
            		console.log(estado);
            		console.log(error);
            	},
            	complete:function(jqXHR,estado){
            		console.log(estado);
            	},
            	timeout:10000
            })
             e.preventDefault();
             console.log("Se oprimio Login");
         });
        
    }
    catch(error){
    	console.log("ERROR EN SUBMIT LOGIN: "+ error);
    }
    
   try{
	   document.getElementById("Link_Carro").onclick=function(){
	    	window.open("CartServlet","_self");
		};
   }
   catch(error){
	   console.log("ERROR EN LINKCARRO: "+error);
   }
});