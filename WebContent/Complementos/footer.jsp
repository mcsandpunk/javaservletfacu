<!DOCTYPE html>
<html lang="en">
<head>    
</head>
<body>
        <footer class="Footer">
                <section class="Footer_Element">
                    <h2>Visit Us</h2>
                    <address>
                        UNIT 1 ASHFORD INDUSTRIAL ESTATE,</br>
                        DIXON STREET,</br>                    
                        WOLVERHAMPTON WV2 2BX,</br>
                        United Kingdom
                    </address>            
                    <article id="Footer_Schedule">
                        <p>Our opening hours are</p>
                        <span>Monday - Friday: <p>9:30 - 17:30</p></span></br>                   
                        <span>Saturday: <p>9:30 - 12:30</p></span>
                        <p>[Saturday subject to appointment in summer months]</p>
                    </article>  
                </section>    
                <section class="Footer_Element">
                    <h2>Find Us</h2>
                    <article id="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1212.356698838548!2d-2.105045483792798!3d52.57479403715932!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48709b98d1d5506b%3A0xc0f3a5a91b0a7e35!2sMGBD%20Parts%20%26%20Services!5e0!3m2!1ses-419!2smx!4v1569434633204!5m2!1ses-419!2smx" width="475rem" height="250rem" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </article>
                </section>        
                <section class="Footer_Element">
                    <h2>Let Us Help You</h2>
                    <article>
                        <p>01902 689975</p>
                        <p>sales@mgbdparts.co.uk</p>
                    </article>
                </section>        
                <section class="Footer_Element">
                    <h2>Online Payments</h2>
                    <article>
                        <p>We accept all major debit and</p>
                        <p>credit cards through our webstore</p>
                        <p>[Except AmEx]</p>
                        <figure>
                            <img src="Recursos/Cards.jpg">
                        </figure>
                    </article>
                </section>        
                <section class="Footer_Element">
                    <h2>Get to Know US</h2>
                    <article>
                        <ul>
                            <li><a href="index2.html">About Us</a></li>
                            <li><a href="index2.html">About Us</a></li>
                            <li><a href="index2.html">About Us</a></li>
                            <li><a href="index2.html">About Us</a></li>
                            <li><a href="index2.html">About Us</a></li>
                        </ul>
                    </article>
                </section>
            </footer>
</body>
</html>