<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>MGBD PARTS ADMIN</title>
<link rel="shortcut icon" href="icono/favicon.ico">
<link rel="stylesheet" type="text/css" href="AdminComple/navbarAdmin.css">
<link rel="stylesheet" type="text/css" href="AdminComple/admin.css">
<script type="text/javascript" src="Recursos/jquery.js"></script>
<script type="text/javascript" src="Recursos/ChartJS/dist/Chart.bundle.min.js"></script>
<script type="text/javascript" src="AdminComple/admin.js"></script>  
</head>
<body>
	<jsp:include page="AdminComple/navbarAdmin.jsp"></jsp:include>
	
	 <article class="Contenido">
            <div class="WrapCanvas">
                <canvas id="Chart_Ganancias"></canvas>
            </div>   
            <div class="WrapCanvas">
                <canvas id="Chart_Visitas"></canvas>
            </div> 
            <div class="WrapCanvas">
                <canvas id="Chart_Productos"></canvas>
            </div>          
        </article>
    </section>
</body>
</html>