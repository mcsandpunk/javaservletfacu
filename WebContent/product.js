$(document).ready(function(){
	
	$(".Product_Image").each(function(index){
		$(this).css("display","none");
		if(index==0){
			$(this).css("display","flex");
		}
	});
	
	try{
	       $(".myVideo").click(function(){
	        $(this).get(0).paused ? $(this).get(0).play() : $(this).get(0).pause();
	       });
	    }catch(error){

	    }
	
	 $(".Slide_Img").click(function(){
	        var Slide = this;
	        
	        $(".Slide_Img").each(function(index){
	            if(Slide==this){
	                var pos=index;
	                $(".Product_Image").each(function(index){
	                    $(this).css("display","none");
	                    if(pos==index){
	                        $(this).css("display","flex");
	                        $(this).css("justify-content","center");
	                    }
	                });
	            }
	        });
	    });
	 
	 $(".checkmark").hover(function(event){  
	        var star = this;
	        $(".checkmark").each(function(index,element){            
	          if(star == this){            
	            for(let i =0;i<=index;i++){
	                $(".checkmark").eq(i).addClass("star_select");
	            }
	          }                
	        })
	      },function(event){
	        var star = this;
	        $(".checkmark").each(function(index,element){            
	          if(star == this){            
	            for(let i =0;i<=index;i++){
	                $(".checkmark").eq(i).removeClass("star_select");
	            }
	          }                
	        })
	          
	      });
});