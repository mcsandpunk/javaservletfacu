<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>MGBD Parts</title>
	<link rel="shortcut icon" href="icono/favicon.ico">
	<link rel="stylesheet" type="text/css" href="Complementos/navbar.css">
	<link rel="stylesheet" type="text/css" href="Complementos/footer.css">
	<link rel="stylesheet" type="text/css" href="Cart.css">
	<script type="text/javascript" src="Recursos/jquery.js"></script>
	<script type="text/javascript" src="Complementos/navbar.js"></script>
	<script type="text/javascript" src="Cart.js"></script>
</head>
<body>
<jsp:include page="Complementos/navbar.jsp"></jsp:include>
<section class="Contenido">
        <article class="Wrap_Cart">
            <h3>Your Cart</h3>      
            <c:choose>
            	<c:when test="${productos!=null && productos>0}">
            	<form class="table_Container" role="table" method="POST" action="CartServlet">
	                <div class="table_header" role="rowgroup">
	                    <div class="cell" role="cell">ID</div>
	                    <div class="cell" role="cell">Product</div>
	                    <div class="cell" role="cell">Quantity</div>
	                    <div class="cell" role="cell">Price</div>
	                    <div class="cell" role="cell">Total</div>
	                    <div class="cell" role="cell">Delete Product</div>
	                </div>
	                <c:forEach var="tempCarrito" items="${carritos}">
	                	<div class="row" role="rowgroup">
		                    <label type="text" name="IDLabel">${tempCarrito.getID_Producto()}</label>                
		                    <a href="ProductInfo?ID=${tempCarrito.getID_Producto()}" class="cell" role="cell">
		                        <div class="image_Container">
		                            <img src="${pageContext.request.contextPath}/ShowImage/${tempCarrito.getImagen()}">
		                        </div>
		                        ${tempCarrito.getArticulo()}
		                    </a>
		                    <input type="text" name="qnt" value="${tempCarrito.getCantidad()}" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
		                    <input type="text" name="sugprice" value="${tempCarrito.getPrecio()}" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
		                    
		                    <label class="Total_Price" readonly="readonly">100</label>
		                    <a href="CartServlet?DeleteID=${tempCarrito.getID_Carrito()}" class="Delete_Icon"><img src="Recursos/Imagenes/declined.png"></a>
		                </div>
		                <input type="hidden" name="Carrito" value="${tempCarrito.getID_Carrito()}">
		                <input type="hidden" name="IDPro" value="${tempCarrito.getID_Producto()}">
	                </c:forEach>                
	                <div class="btn_Dec">
	                    <input  class="btn_Send" type="submit" value="Send Request" name="Send">
	                    <input  class="btn_Declined" type="submit" value="Erase All" name="Send">
	                    <input  class="btn_Update" type="submit" value="Update Cart" name="Send">
	                   <!--  <a class="btn_Declined"><p>Erase All</p></a>
	                    <a class="btn_Update"><p>Update Cart</p></a>--> 
	                </div>
	            </form>
            	</c:when>
            	<c:otherwise>
            		 <h4>Your Cart is Empty</h4>
            	</c:otherwise>
            </c:choose>
           
            
        </article>
    </section>
<jsp:include page="Complementos/footer.jsp"></jsp:include>
</body>
</html>