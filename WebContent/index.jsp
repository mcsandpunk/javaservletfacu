<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>MGBD Parts</title>
	<link rel="shortcut icon" href="icono/favicon.ico">
	<link rel="stylesheet" type="text/css" href="Complementos/navbar.css">
	<link rel="stylesheet" type="text/css" href="Complementos/footer.css">
	<link rel="stylesheet" type="text/css" href="index.css">
	<script type="text/javascript" src="Recursos/jquery.js"></script>
    <script type="text/javascript" src="Complementos/navbar.js"></script>
    <script type="text/javascript" src="index.js"></script>
</head>
<body>
<jsp:include page="Complementos/navbar.jsp"></jsp:include>
    <section class="Contenido">
        <article class="Slider_Carrousel">
            <div class="Slider_Container">
           <c:forEach var="temSlide" items="${ImagenesSlide}">
            	<div class="Slide fade">
                    <figure>                    	
                        <img src= "GetImageInfo?Dec=1&ID=${temSlide.getID()}" >
                    </figure>
                </div>  
            </c:forEach>  
            </div> 
            <a class="Slide_prev" onclick="changeSlide(-1)">&#10094;</a>
            <a class="Slide_next" onclick="changeSlide(+1)">&#10095;</a>               
        </article>
        <article class="SubMenu">
            <section class="SubMenu_Parts">
                <h1 class="SubMenu_Title">New Products</h1>
                <hr class="SubMenu_Divisor">      
                <section class="SubMenu_Slider_Container">
                    <section class="SubMenu_Slider">
                    <c:forEach var="tempCard" items="${ListaProductos}">
                   		<article value ="${tempCard.getID()}" class="Card">                    
                          <a href="ProductInfo?ID=${tempCard.getID()}"><img class="Card_Img" src="${pageContext.request.contextPath}/ShowImage/${tempCard.getDireccionImagen()}">
                           <p class="Card_Title"><c:out value="${tempCard.getArticulo()}"></c:out> </p>
                           
                           <p class="Card_Description">${tempCard.getDescripcion()}</p>
                           </a>                
                           <button value="${tempCard.getID()}"class="btn_Card_AddCart">Add to Cart
                           		<span class="PopUp">Please Login or SignUp</span>
                           </button>
                       	</article>
                    </c:forEach>   
                    </section>
                    <section class="SubMenu_Slider">
                       <c:forEach var="tempCard" items="${ListaProductos}">
               				<article class="Card">                    
	                           <a href="ProductInfo?ID=${tempCard.getID()}"><img class="Card_Img" src="${pageContext.request.contextPath}/ShowImage/${tempCard.getDireccionImagen()}">
	                           <p class="Card_Title"><c:out value="${tempCard.getArticulo()}"></c:out> </p>
	                          
	                           <p class="Card_Description">${tempCard.getDescripcion()}</p>
	                           </a>               
	                           <button value="${tempCard.getID()}"class="btn_Card_AddCart">Add to Cart
	                           		<span class="PopUp">Please Login or SignUp</span>
	                           </button>
                       		</article>
                    	</c:forEach>                         
                    </section>
                    <section class="SubMenu_Slider">
                       <c:forEach var="tempCard" items="${ListaProductos}">
	                   		<article class="Card">                    
	                           <a href="ProductInfo?ID=${tempCard.getID()}"><img class="Card_Img" src="${pageContext.request.contextPath}/ShowImage/${tempCard.getDireccionImagen()}">
	                           <p class="Card_Title"><c:out value="${tempCard.getArticulo()}"></c:out> </p>
	                          
	                           <p class="Card_Description">${tempCard.getDescripcion()}</p>
	                           </a>               
	                           <button value="${tempCard.getID()}"class="btn_Card_AddCart">Add to Cart
	                           		<span class="PopUp">Please Login or SignUp</span>
	                           </button>
	                       	</article>
                    	</c:forEach>                        
                    </section> 
                        <button  class="SubMenu_Slider_prev">&#10094;</button> 
                        <button  class="SubMenu_Slider_next">&#10095;</button>                     
                </section>                                           
            </section>           
            <section class="SubMenu_Parts">
                <h1 class="SubMenu_Title">Best Selling</h1>
                <hr class="SubMenu_Divisor">      
                <section class="SubMenu_Slider_Container">
                    <section class="SubMenu_Slider">
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/539355_m.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"></p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>   
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/553802r.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"></p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/578994.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"></p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/84221.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"></p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/MGBD0434.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"></p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/RTC0700.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"></p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>
                    </section>
                    <section class="SubMenu_Slider">
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/539355_m.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"></p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>   
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/553802r.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"></p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/578994.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"></p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/84221.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"> �45.00 </p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/MGBD0434.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"> �14.00 </p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/RTC0700.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"> �25.00 </p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>
                    </section>
                    <section class="SubMenu_Slider">
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/539355_m.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"> �7.00 </p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>   
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/553802r.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"> �1.00 </p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/578994.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"> �78.00 </p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/84221.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"> �45.00 </p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/MGBD0434.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"> �14.00 </p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>
                        <article class="Card">                    
                            <img class="Card_Img" src="Recursos/Productos/Cooling/RTC0700.png">
                            <p class="Card_Title">TOP RADIATOR HOSE P6 2000 (1963-67) [NOS]</p>
                            <p class="Card_Price"> �25.00 </p>
                            <p class="Card_Description">THESE HOSES ARE NEW OLD STOCK AND ARE REINFORCED LIKE OE PARTS. </p>               
                            <button class="btn_Card_AddCart">Add to Cart</button>
                        </article>
                    </section>                                                           
                    <button class="SubMenu_Slider_prev">&#10094;</button> 
                    <button class="SubMenu_Slider_next">&#10095;</button>                                         
                </section>                
            </section>

        </article>
    </section>
<jsp:include page="Complementos/footer.jsp"></jsp:include>
</body>
</html>