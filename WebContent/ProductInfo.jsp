<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>MGBD Parts</title>
    <link rel="shortcut icon" href="icono/favicon.ico">
    <link rel="stylesheet" type="text/css" href="Complementos/navbar.css">
	<link rel="stylesheet" type="text/css" href="Complementos/footer.css">
    <link rel="stylesheet" type="text/css" href="product.css">
    <script type="text/javascript" src="Recursos/jquery.js"></script>
    <script type="text/javascript" src="Complementos/navbar.js"></script>
    <script type="text/javascript" src="product.js"></script> 
</head>
<body>
<jsp:include page="Complementos/navbar.jsp"></jsp:include>
<section class="Contenido">
        <section class="Wrapper_Contenido">
            <article class="Wrapper_Producto">
                <div class="Product_Side">                    
                    <div class="Wrapper_Slider_Imagen">
                       <div class="Slider_Imagen">
                       <c:forEach var="tempSlide" items="${ImgPro}">
                        	<div class="Slide_Img" >
                                <img src="${pageContext.request.contextPath}/ShowImage/${tempSlide.getLocalizacion()}">
                           </div>
                       </c:forEach>
                       <c:forEach var="tempVideo" items="${VideosPro}">
                         <div class="Slide_Img">
	                        	<video>
	                        		<source src="${pageContext.request.contextPath}/ShowImage/${tempVideo.getDireccion()}" type="video/mp4">
	                        	</video>
                        	</div> 
                       </c:forEach>                 	             
                       </div>
                    </div>
                    <div class="Wrapper_Product_Imagen">
                    <c:forEach var="ImgProduct" items="${ImgPro}">
                    	<div class="Product_Image">
                            <img src="${pageContext.request.contextPath}/ShowImage/${ImgProduct.getLocalizacion()}">
                        </div>
                    </c:forEach>
                    <c:forEach var="VidPro" items="${VideosPro}">
                    	 <div class="Product_Image" style="display:none;">
                        	<video class="myVideo">
                        		<source src="${pageContext.request.contextPath}/ShowImage/${VidPro.getLocalizacion()}" type="video/mp4">
                        	</video>
                        </div> 
                    </c:forEach>
                                              
                    </div>  
                </div>
                <div class="Description_Side">
                    <div class="Wrapper_Description_Side">
                        <div class="Rating_Stars">
                       	<c:choose>
	            		 	<c:when test="${Producto.getRating() <0 }">
	            		 		<span style="font-size:1.5em">No Rated Yet</span>
	            		 	</c:when>
	            		 	<c:otherwise>
	            		 		<c:forEach begin="1" end="${Producto.getRating()}">
	             					<span class="Star"></span>
	             				</c:forEach> 
	            		 	</c:otherwise>
                     	</c:choose>                                              
                        </div>
                        <div class="Description_Categories">
                            <h3 class="Categorie">${Producto.getsCategoria()}</h3>
                            <h3 class="Part">${Producto.getsPart()}</h3>
                        </div>
                        <h2 class="Description_Title">${Producto.getArticulo()}</h2>
                        <p class="Description_Main">${Producto.getDescripcion()}                         
                        </p>
                    </div>
                    <div class="Wrapper_Description_Buttons">
                        <button class="btn_AddCart" type="submit">Add to Cart</button>
                        
                    </div>
                </div>
            </article>
            <article class="Wrapper_Extras">
                <div class="Wrapper_Valoracion">
                    <div class="star_rating">
                        <p>User Rating</p>
                        <label class="Option_Rating">
                            <input type="checkbox" checked="checked">
                            <span class="checkmark"></span>
                        </label>
                        <label class="Option_Rating">
                            <input type="checkbox" checked="checked">
                            <span class="checkmark"></span>
                        </label>
                        <label class="Option_Rating">
                            <input type="checkbox" checked="checked">
                            <span class="checkmark"></span>
                        </label>
                        <label class="Option_Rating">
                            <input type="checkbox" checked="checked">
                            <span class="checkmark"></span>
                        </label>
                        <label class="Option_Rating">
                            <input type="checkbox" checked="checked">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
                <div class="Wrapper_Comentario">
                    <form class="Form_Comentario" autocomplete=off>
                        <textarea class="Comentario" required></textarea>
                        <button type="submit">Send Comment</button>
                    </form>
                    <p>Customer Reviews</p>
                    <section class="Wrapper_Comments">
                        <div class="Comment">
                            <div class="User_Image">
                                <img src="Recursos/Avatars/512/4043233 - anime away face no nobody spirited.png">
                            </div>
                            <div class="Wrapper_Text">
                                <div class="triangle"></div>
                                <div class="Content">
                                    <article class="Comment_header">
                                        <p class="Comment_Nick">Nicolas</p>
                                        <p class="Comment_Date">25/08/2015</p>
                                        <div class="Rating_Stars">
                                            <span class="Star"></span>
                                            <span class="Star"></span>
                                            <span class="Star"></span>
                                            <span class="Star"></span>
                                            <span class="Star"></span>
                                        </div>
                                    </article>
                                    <p class="Comment_Text">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nobis facilis cumque animi consequuntur ullam ex a reiciendis illo suscipit assumenda enim sapiente ad similique mollitia iusto neque vitae aliquam ab, voluptas quas voluptates asperiores error pariatur doloribus? Culpa, doloremque sint, nisi consequuntur quidem alias illo, sapiente accusamus quasi molestias modi.</p>
                                </div>
                            </div>
                        </div>
                        <div class="Comment">
                            <div class="User_Image">
                                <img src="Recursos/Avatars/512/4043244 - avatar cloud crying rain.png">
                            </div>
                            <div class="Wrapper_Text">
                                <div class="triangle"></div>
                                <div class="Content">
                                    <article class="Comment_header">
                                        <p class="Comment_Nick">Nicolas</p>
                                        <p class="Comment_Date">25/08/2015</p>
                                        <div class="Rating_Stars">
                                            <span class="Star"></span>
                                            <span class="Star"></span>
                                            <span class="Star"></span>
                                            <span class="Star"></span>
                                            <span class="Star"></span>
                                        </div>
                                    </article>
                                    <p class="Comment_Text">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Repudiandae possimus esse cum, ipsam ducimus consequatur unde dolores placeat aut. Cum?</p>
                                </div>
                            </div>
                        </div>
                        <div class="Comment">
                            <div class="User_Image">
                                <img src="Recursos/Avatars/512/4043229 - afro avatar male man.png">
                            </div>
                            <div class="Wrapper_Text">
                                <div class="triangle"></div>
                                <div class="Content">
                                    <article class="Comment_header">
                                        <p class="Comment_Nick">Nicolas</p>
                                        <p class="Comment_Date">25/08/2015</p>
                                        <div class="Rating_Stars">
                                            <span class="Star"></span>
                                            <span class="Star"></span>
                                            <span class="Star"></span>
                                            <span class="Star"></span>
                                            <span class="Star"></span>
                                        </div>
                                    </article>
                                    <p class="Comment_Text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor quasi sapiente iste dignissimos minus sunt, iusto unde deserunt illum suscipit necessitatibus voluptatibus mollitia officiis in consectetur fugit porro et dolores. Id, itaque? At quas explicabo omnis dolorum dicta ullam hic, quidem debitis. Non aperiam placeat voluptas rem possimus repellendus. Cum tempora illo, eos nisi fugiat dolores, aperiam quos tenetur repudiandae praesentium deleniti ad aliquid similique ab? Quo vel magnam architecto adipisci, quaerat nesciunt illum nam necessitatibus molestiae et maiores tempore eos laudantium soluta! Earum minima provident illo, excepturi soluta fugit officia veritatis molestias rem id autem eius delectus beatae nostrum sed quasi odit consequuntur. Magnam velit quasi quos fugit optio qui iure laboriosam sint. Qui sequi possimus cum odio nobis ullam, eos alias voluptates nulla laudantium asperiores. Repellendus ut saepe, dolore sit recusandae aliquid consequuntur rerum ullam voluptas dignissimos repellat explicabo. Necessitatibus possimus saepe blanditiis totam optio repellendus enim modi.</p>
                                </div>
                            </div>
                        </div>
                    </section>
                    
                    
                </div>
            </article>
        </section>
    </section>
<jsp:include page="Complementos/footer.jsp"></jsp:include>
</body>
</html>