$(document).ready(function(){
	

	    
	$("#Add_Submit").click(function(e){
		
		var act = $("#AddProduct").attr("action");
        var met = $("#AddProduct").attr("method");
        e.preventDefault();
        var datos = new FormData($("#AddProduct")[0]);
        var info = $("#Add_Info");
        let Add_Name = $("#Add_Name");
        let Add_Desc = $("#Add_Desc");
        let Add_Quant = $("#Add_Quant");
        let Add_Price =$("#Add_Price");
        let Product_Image = $("#Add_Image");
        $.ajax({
        	url:act,
        	type:met,
        	contentType:false,
        	processData:false,
        	cache:false,
        	data:datos,
        	success:function(resp){
        		console.log("success Add");
        		console.log(resp);
        		switch(resp){
        		case "NombCorto":        			
        			Add_Name.css("border","0.5rem solid red");
        			Add_Desc.css("border","none");
        			Add_Quant.css("border","none");
        			Product_Image.css("border","none");
        			info.text("Too small Name");
        			
        			break;
        		case "DescCorto":        			
        			Add_Desc.css("border","0.5rem solid red");        			
        			Add_Name.css("border","none");
        			Add_Quant.css("border","none");
        			Product_Image.css("border","none");
        			
        			info.text("Too small Description");
        			break;
        		case "NoNumber":        			
        			Add_Quant.css("border","0.5rem solid red");
        			Add_Name.css("border","none");
        			Product_Image.css("border","none");
        			Add_Desc.css("border","none");
        			
        			info.text("It's not a number");
        			break;
        		case "NoImageVideo":
        			Product_Image.css("border","0.5rem solid red");
        			Add_Name.css("border","none");
        			Add_Quant.css("border","none");
        			Add_Desc.css("border","none");
        			
        			info.text("Select an Image or Video");
        			break;
        		case "SizeBig":
        			Product_Image.css("border","0.5rem solid red");
        			Add_Name.css("border","none");
        			Add_Quant.css("border","none");
        			Add_Desc.css("border","none");
        			
        			info.text("The Image(s) are so big");
        			break;
        		case "NoNumberPrice":
        			Add_Price.css("border","0.5rem solid red");
        			Add_Name.css("border","none");
        			Add_Quant.css("border","none");
        			Add_Desc.css("border","none");
        			Product_Image.css("border","0.5rem solid red");
        			
        			info.text("The price seems not correct");
        			break;
        		case "Nice":
        			window.open("ProductsAdminServlet","_self");
        			break;
        		}
        	},error:function(jqHXR,estado,error){
        		console.log(estado);
        		console.log(error);
        	},
        	complete:function(jqXHR,estado){
        		console.log(estado);
        	},
        	timeout:10000
        	
        });
        
        console.log("Pasa por Ajax");
	})
	
	$("#Edit_Submit").click(function(e){
		
		var act = $("#Form_EditProduct").attr("action");
        var met = $("#Form_EditProduct").attr("method");
        e.preventDefault();
        var datos = new FormData($("#Form_EditProduct")[0]);
        var info = $("#Edit_Info");
        let Add_Name = $("#Edit_Name");
        let Add_Desc = $("#Edit_Desc");
        let Add_Quant = $("#Edit_Quant");
        let Product_Image = $("#Edit_Image");
        $.ajax({
        	url:act,
        	type:met,
        	contentType:false,
        	processData:false,
        	cache:false,
        	data:datos,
        	success:function(resp){
        		console.log("success Edit");
        		console.log(resp);
        		switch(resp){
        		case "NombCorto":        			
        			Add_Name.css("border","0.5rem solid red");
        			Add_Desc.css("border","none");
        			Add_Quant.css("border","none");
        			Product_Image.css("border","none");
        			info.text("Too small Name");
        			
        			break;
        		case "DescCorto":        			
        			Add_Desc.css("border","0.5rem solid red");        			
        			Add_Name.css("border","none");
        			Add_Quant.css("border","none");
        			Product_Image.css("border","none");
        			
        			info.text("Too small Description");
        			break;
        		case "NoNumber":        			
        			Add_Quant.css("border","0.5rem solid red");
        			Add_Name.css("border","none");
        			Product_Image.css("border","none");
        			Add_Desc.css("border","none");
        			
        			info.text("It's not a number");
        			break;
        		case "NoImageVideo":
        			Product_Image.css("border","0.5rem solid red");
        			Add_Name.css("border","none");
        			Add_Quant.css("border","none");
        			Add_Desc.css("border","none");
        			
        			info.text("Select an Image or Video");
        			break;
        		case "SizeBig":
        			Product_Image.css("border","0.5rem solid red");
        			Add_Name.css("border","none");
        			Add_Quant.css("border","none");
        			Add_Desc.css("border","none");
        			
        			info.text("The Image(s) are so big");
        			break;
        		case "NoNumberPrice":
        			Add_Price.css("border","0.5rem solid red");
        			Add_Name.css("border","none");
        			Add_Quant.css("border","none");
        			Add_Desc.css("border","none");
        			Product_Image.css("border","0.5rem solid red");
        			
        			info.text("The price seems not correct");
        			break;
        		case "Nice":
        			window.open("ProductsAdminServlet","_self");
        			break;
        		}
        	},error:function(jqHXR,estado,error){
        		console.log(estado);
        		console.log(error);
        	},
        	complete:function(jqXHR,estado){
        		console.log(estado);
        	},
        	timeout:10000
        	
        });
        
        console.log("Pasa por Ajax EDIT");
	})
	

	  try{
	       $(".myVideo").click(function(){
	        $(this).get(0).paused ? $(this).get(0).play() : $(this).get(0).pause();
	       });
	    }catch(error){

	    }
	
})