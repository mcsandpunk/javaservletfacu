<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>	

    <navbar class="navAdmin">
        <div class="Logo">
            <a href="AdminServlet" style="text-decoration:none;display:inline;">
            	<img src="icono/Logo.png"> 
            	<p class="Title"><c:out value="${UserInfo.getNickname()} Dashboard"/> </p>
            </a>
        </div>
        <div class="User">
            <ul>
                <li>
                    <div class="UserImg">
                        <img src="GetImageInfo?Dec=2&ID=${UserInfo.getID()}">
                    </div>
                    <label><c:out value="${UserInfo.getNickname()}"/>
                        <ul>
                            <li><a href="ProfileServlet">Settings</a></li>
                            <li ><a href="NavbarAdminServlet?LogOut=TRUE">LogOut</a></li>
                        </ul>
                    </label>
                </li>
            </ul>
        </div>
    </navbar>
    <section class="Wrapper">
        <aside class="Wrap_Opciones">
            <div class="Opciones">
                 <div class="Menu">
                    <input id="Profile" type="checkbox"> 
                    <label for="Profile"class="Item">Profile</label>    
                    <ul class="subMenu">
                        <li class="subItem"><a href="ProfileServlet#Settings">Settings</a></li>
                        <li class="subItem"><a href="ProfileServlet#ModifySlide">Modify Slide</a></li>
                    </ul>
                    <input id="Products" type="checkbox">
                    <label for="Products" class="Item">Products</label>
                    <ul class="subMenu">
                        <li class="subItem"><a href="ProductsAdminServlet#AddProducts">Add Products</a></li>
                        <li class="subItem"><a href="ProductsAdminServlet#EditProducts">Edit Products</a></li>                        
                    </ul>
                    <input id="Users" type="checkbox">
                    <label for="Users" class="Item">Users</label>
                    <ul class="subMenu">
                        <li class="subItem"><a href="#ModifyUsers">Modify Users</a></li>
                    </ul>
                    <input id="Request" type="checkbox">
                    <label for="Request" class="Item">Request</label>
                    <ul class="subMenu">
                        <li class="subItem"><a href="RequestServlet#New">New</a></li>
                        <li class="subItem"><a href="RequestServlet#Approved">Approved</a></li>
                        <li class="subItem"><a href="RequestServlet#Denied">Denied</a></li>
                    </ul>
                    <input id="Statistics" type="checkbox">
                    <label for="Statistics" class="Item">Statistics</label>
                    <ul class="subMenu">
                        <li class="subItem"><a href="#Products">Products</a></li>
                        <li class="subItem"><a href="#Users">Users</a></li>
                        <li class="subItem"><a href="#Requests">Requests</a></li>
                        <li class="subItem"><a href="#Views">Views</a></li>
                    </ul>
                 </div>
            </div>
        </aside>