<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>MGBD Parts ADMIN</title>
    <link rel="shortcut icon" href="icono/favicon.ico">
    <c:choose>
    	<c:when test="${UserInfo.getNivel()==0}">
    		<link rel="stylesheet" type="text/css" href="AdminComple/navbarAdmin.css">
    		<link rel="stylesheet" type="text/css" href="AdminComple/admin.css">
    		
    	</c:when>
    	<c:otherwise>
    		<link rel="stylesheet" type="text/css" href="Complementos/navbar.css">
		 	<link rel="stylesheet" type="text/css" href="UserSettings.css">  
		 	
    	</c:otherwise>
    </c:choose>   
    <script type="text/javascript" src="Recursos/jquery.js"></script>
    <script type="text/javascript" src="Settings.js"></script>
    <script type="text/javascript" src="navbar.js"></script>
</head>
<body>
	<c:choose>
		<c:when test="${UserInfo.getNivel()==0}">
			<jsp:include page="AdminComple/navbarAdmin.jsp"></jsp:include>
		</c:when>
		<c:otherwise>
			<jsp:include page="Complementos/navbar.jsp"></jsp:include>
			 <section class="Wrapper">
        <aside class="Wrap_Opciones">
            <div class="Opciones">
                <div class="Menu">
                    <input id="Profile" type="checkbox"> 
                    <label for="Profile"class="Item">Profile</label>    
                    <ul class="subMenu">
                        <li class="subItem"><a href="ProfileServlet">Settings</a></li>                        
                    </ul>                 
                    <input id="Request" type="checkbox">
                    <label for="Request" class="Item">Request</label>
                    <ul class="subMenu">
                        <li class="subItem"><a href="RequestServlet#New">Stand By</a></li>
                        <li class="subItem"><a href="RequestServlet#Approved">Approved</a></li>
                        <li class="subItem"><a href="RequestServlet#Denied">Denied</a></li>
                    </ul>
                    <input id="Shopping" type="checkbox">
                    <label for="Shopping" class="Item">Shopping</label>
                    <ul class="subMenu">
                        <li class="subItem"><a href="#New">Shopping History</a></li>                        
                    </ul>
                </div>
            </div>
        </aside>
		</c:otherwise>
	</c:choose>

     <article class="Contenido">
                <h2 class="Contenido_Title">Settings Profile</h2>
                <form class="Form_Settings" id="Settings" action="ProfileServlet" method="POST" enctype="multipart/form-data">
                    <h3>Personal Info</h3>
                    <label for="Name_User">Name</label>
                    <input id="Name_User" type="text" name="Name_User" value="${UserInfo.getNombre()}">
                    
                    <label for="LastName">Last Name</label>
                    <input id="LastName" type="text" name="Last_Name" value="${UserInfo.getApellido()}">
                    
                    <label for="Email">Email</label>
                    <input id="Email" type="text" name="Email" value="${UserInfo.getEmail()}">
                    
                    <label for="NickName">Nick Name</label>
                    <input id="NickName" type="text" name="NickName" value="${UserInfo.getNickname()}">
                    
                    <h3>Change Password</h3>
                    <label for="Pass">Password</label>
                    <input id="Pass" type="password" name="pass">
                    <label for="RePass">Repeat Password</label>
                    <input id="RePass" type="password" name="repass">
                    
                    <h3>Change Image Profile</h3>
                    <label for="Image">Current Image</label>
                    <div class="Image_Container">
                        <img src="GetImageInfo?Dec=2&ID=${UserInfo.getID()}">
                    </div>
                    <label for="Image">Change Image</label>
                    <input id="Image" type="file" name="ImageUser"  accept=".png, .jpeg, .jpg">

                    <input name="Send" id="submit" type="submit" value="Update Info">
                </form>
                <c:if test="${UserInfo.getNivel()==0}">
                <h3>Modify Slide</h3>
                <article id="ModifySlide" class="ImageSlide">                    
                    <c:forEach var="tempSlide" items="${Imagenes}">
                    	<div class="Image_Container">
                        <div class="Option_Delete">
                            <div value="${tempSlide.getID()}" class="Icon_Container">
                                <img src="Recursos/Imagenes/Trash.png">
                                <label>Delete Image</label>                                
                            </div>
                        </div>
                        	<img src="GetImageInfo?Dec=1&ID=${tempSlide.getID()}">
                    	</div>
                    </c:forEach> 
                    <div class="Image_Container">
                        <form class="Option_Add" action="ProfileServlet" method="POST" enctype="multipart/form-data">
                            <img src="Recursos/Imagenes/Add.png">
                            <label for="ImageSlide_input">Select Image</label>
                            <input id="ImageSlide_input" type="file" name="ImageSlide"  accept=".png, .jpeg, .jpg">                           
                            <input style="display:block;color:whitesmoke;" id="btn_sub" name="btn_send"type="submit" value="Add Image">
                        </form>                    
                    </div> 
                    <h4 id="Aviso">YOU CAN'T DELETE THE LAST TWO IMAGES</h4>                
                </article>
                </c:if>
                
            </article>
        </section>
</body>
</html>