$(document).ready(function(){
    

	
    /**************************************************************/
    /********************************SLIDE*************************/
    /**************************************************************/

    var slideIndex = -1;
    var slideIndexID=-1;
    var veces=0;

    showSlides();

    /***************CUANDO EL MOUSE ESTE ENCIMA*******************/
    $(".Slider_Carrousel").mouseenter(function(){
        clearInterval(slideIndexID);
        slideIndexID=-1;
    });

    $(".Slider_Carrousel").mouseleave(function(){
        showSlides();
    })

    function changeSlide(n){
        slideIndex+=n;
        if(slideIndexID!=-1){
            clearInterval(slideIndexID);
            slideIndexID=-1;
        }
        showSlides();
        clearInterval(slideIndexID);
        slideIndexID=-1;
    }

    
    document.getElementsByClassName("Slide_next")[0].onclick=function(){
        changeSlide(+1);
     }
 
     document.getElementsByClassName("Slide_prev")[0].onclick=function(){
         changeSlide(-1);
     }


    function showSlides(){       
       // console.log(slideIndex) ;
        /*slideIndex++;*/
        let x= document.getElementsByClassName("Slide");
        //console.log(x.length);
        if(slideIndex>x.length-1){
            //console.log("SlideIndexMayor");
            slideIndex=0;
        }
            
        if(slideIndex<0){
            //console.log("SlideIndexMenor")
            slideIndex = x.length-1;
        }

       // console.log("SlideIndexDES: "+slideIndex);

        //console.log("Veces por aqui: "+veces);
        veces++;
        
        for(let i = 0;i<x.length;i++){            
            x[i].style.display = "none"
        }

        x[slideIndex].style.display = "block";
        
       if(slideIndexID==-1){
            slideIndexID=setInterval(function(){
                slideIndex++;
                showSlides();
            },1000);
        }
    }

    /********************************************************/
    /*********************PRODUCT SLIDER********************/
    /*******************************************************/

    var mouseDownID=-1;

     function mouseDown(event,data){
         console.log("Entra Mouse Down");
         if(mouseDownID==-1){
             console.log("MouseDownID entra");
            mouseDownID = setInterval(function(){whileMouseDown(event,data)},250);            
         }            
     }

     function mouseUp(event){
         if(mouseDownID!=-1){
             clearInterval(mouseDownID);
             mouseDownID=-1;
         }
     }

     function whileMouseDown(event,data){
        data.posFinal+=event;
        console.log("PosInicial:"+data.posInicial);
        console.log("PosFinal:"+data.posFinal);
                
        var v=[{x:data.posInicial},{x:data.posFinal}];
        MoverSlide(v,data);
        data.posInicial=data.posFinal;                

        if(data.posFinal==(event*10)){
          data.posFinal=0;
          data.posInicial=0;
        }
    };

    function MoverSlide(v,data){         
        $(v[0]).animate(v[1],{
            duration: 250,
            step: function(val){
                
            for(let i=data.Inicio;i<data.Fin;i++){
                data.Elemento.eq(i).css("transform",`translateX(${val}%)`);
            }  
            }
        })
     };  
     

     /*var Primer = $(".SubMenu_Slider:eq(0)").children(".Card:first");*/
     var Primeros3Elementos={
         Elemento:$(".SubMenu_Slider"),
         Inicio:0,
         Fin:3,
         posInicial:0,
         posFinal:0
     };
     var Ultimos3Elementos={
        Elemento:$(".SubMenu_Slider"),
        Inicio:3,
        Fin:6,
        posInicial:0,
        posFinal:0
    };

    document.getElementsByClassName('SubMenu_Slider_next')[0].onmousedown = function(){
         whileMouseDown(-10,Primeros3Elementos);
     };

    document.getElementsByClassName('SubMenu_Slider_prev')[0].onmousedown = function(){
        whileMouseDown(10,Primeros3Elementos);
    };

    document.getElementsByClassName('SubMenu_Slider_next')[0].addEventListener("mousedown",function(){mouseDown(-10,Primeros3Elementos)}); 
    document.getElementsByClassName('SubMenu_Slider_next')[0].addEventListener("mouseup",mouseUp); 
    document.getElementsByClassName('SubMenu_Slider_next')[0].addEventListener("mouseout",mouseUp); 
    
    
    document.getElementsByClassName('SubMenu_Slider_prev')[0].addEventListener("mousedown",function(){mouseDown(10,Primeros3Elementos)});
    document.getElementsByClassName('SubMenu_Slider_prev')[0].addEventListener("mouseup",mouseUp); 
    document.getElementsByClassName('SubMenu_Slider_prev')[0].addEventListener("mouseout",mouseUp); 

    document.getElementsByClassName('SubMenu_Slider_next')[1].onmousedown = function(){
        whileMouseDown(-10,Ultimos3Elementos);
    };

   document.getElementsByClassName('SubMenu_Slider_prev')[1].onmousedown = function(){
       whileMouseDown(10,Ultimos3Elementos);
   };

   document.getElementsByClassName('SubMenu_Slider_next')[1].addEventListener("mousedown",function(){mouseDown(-10,Ultimos3Elementos)}); 
   document.getElementsByClassName('SubMenu_Slider_next')[1].addEventListener("mouseup",mouseUp); 
   document.getElementsByClassName('SubMenu_Slider_next')[1].addEventListener("mouseout",mouseUp); 
   
   
   document.getElementsByClassName('SubMenu_Slider_prev')[1].addEventListener("mousedown",function(){mouseDown(10,Ultimos3Elementos)});
   document.getElementsByClassName('SubMenu_Slider_prev')[1].addEventListener("mouseup",mouseUp); 
   document.getElementsByClassName('SubMenu_Slider_prev')[1].addEventListener("mouseout",mouseUp); 
    
});