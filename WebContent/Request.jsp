<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    
<title>MGBD Parts</title>
<link rel="shortcut icon" href="icono/favicon.ico">
	<c:choose>
		<c:when test="${UserInfo.getNivel()==0}">
			<link rel="stylesheet" type="text/css" href="AdminComple/navbarAdmin.css">		
		</c:when>
		<c:otherwise>
			<link rel="stylesheet" type="text/css" href="Complementos/navbar.css">
		</c:otherwise>
	</c:choose>
<link rel="stylesheet" type="text/css" href="Request.css">
<script type="text/javascript" src="Recursos/jquery.js"></script>
<script type="text/javascript" src="Request.js"></script>
</head>
<body>
<c:choose>
		<c:when test="${UserInfo.getNivel()==0}">
			<jsp:include page="AdminComple/navbarAdmin.jsp"></jsp:include>
		</c:when>
		<c:otherwise>
			<jsp:include page="Complementos/navbar.jsp"></jsp:include>
			 <section class="Wrapper">
        <aside class="Wrap_Opciones">
            <div class="Opciones">
                <div class="Menu">
                    <input id="Profile" type="checkbox"> 
                    <label for="Profile"class="Item">Profile</label>    
                    <ul class="subMenu">
                        <li class="subItem"><a href="ProfileServlet">Settings</a></li>                        
                    </ul>                 
                    <input id="Request" type="checkbox">
                    <label for="Request" class="Item">Request</label>
                    <ul class="subMenu">
                        <li class="subItem"><a href="RequestServlet#Pending_Req">Stand By</a></li>
                        <li class="subItem"><a href="RequestServlet#Approved_Req">Approved</a></li>
                        <li class="subItem"><a href="RequestServlet#Declined_Req">Denied</a></li>
                    </ul>
                    <input id="Shopping" type="checkbox">
                    <label for="Shopping" class="Item">Shopping</label>
                    <ul class="subMenu">
                        <li class="subItem"><a href="#New">Shopping History</a></li>                        
                    </ul>
                </div>
            </div>
        </aside>
		</c:otherwise>
	</c:choose>
	
	<section class="Contenido">
            <article class="Wrap_Options">
                <a href="#">
                    <div class="Option_Container">
                        <div class="Icon_Container">
                            <img src="Recursos/Imagenes/total.png">
                        </div>
                        <div class="Descrip">
                            <p class="Descrip_Number">${TotalRequest}</p>
                            <p class="Descrip_Text">Total Request</p>
                        </div>
                    </div>
                </a>
                <a href="RequestServlet#Pending_Req">
                    <div class="Option_Container">
                        <div class="Icon_Container">
                            <img src="Recursos/Imagenes/new.png">
                        </div>
                        <div class="Descrip">
                            <p class="Descrip_Number">${TotalPendingRequest}</p>
                            <p class="Descrip_Text">Pending Request</p>
                        </div>
                    </div>
                </a>
                <a href="RequestServlet#Declined_Req">
                    <div class="Option_Container">
                        <div class="Icon_Container">
                            <img src="Recursos/Imagenes/declined.png">
                        </div>
                        <div class="Descrip">
                            <p class="Descrip_Number">${TotalDeclinedRequest}</p>
                            <p class="Descrip_Text">Declined Request</p>
                        </div>
                    </div>
                </a>
                <a href="RequestServlet#Approved_Req">
                    <div class="Option_Container">
                        <div class="Icon_Container">
                            <img src="Recursos/Imagenes/approved.png">
                        </div>
                        <div class="Descrip">
                            <p class="Descrip_Number">${TotalApproRequest}</p>
                            <p class="Descrip_Text">Approved Request</p>
                        </div>
                    </div>
                </a>
                </div>
            </article>
            <article class="Wrap_Request">
                <h3 id="Pending_Req">Pending Request</h3>
                <div class="List_Request" role="table">
                    <div class="header_Request row" role="rowgroup">
                        <div class="cell" role="columnheader"><a href="#">Request Num</a></div>
                        <c:choose>
                        	<c:when test="${UserInfo.getNivel()==0}">
                        		<div class="cell" role="columnheader"><a href="#">Customer NickName</a></div>
                        	</c:when>
                        	<c:otherwise>
                        		<div class="cell" role="columnheader"><a href="#">Quantity</a></div>
                        	</c:otherwise>
                        </c:choose>
                        <div class="cell" role="columnheader"><a href="#">Total</a></div>
                        <div class="cell" role="columnheader"><a href="#">Sent Date</a></div>
                    </div>
                    <c:forEach var="tempRow" items="${pendientes}">
                   	<div class="row" role="rowgroup">
                        <div class="cell" role="cell"><a href="RequestServlet?ID_S=${tempRow.getID_Solicitud()}">${tempRow.getID_Solicitud()}</a></div>
                        <c:choose>
                        	<c:when test="${UserInfo.getNivel()==0}">
                        		<div class="cell" role="cell"><a href="RequestServlet?ID_S=${tempRow.getID_Solicitud()}">${tempRow.getNick()}</a></div>
                        	</c:when>
                        	<c:otherwise>
                        		<div class="cell" role="cell"><a href="RequestServlet?ID_S=${tempRow.getID_Solicitud()}">${tempRow.getCantidadProductos()}</a></div>
                        	</c:otherwise>
                        </c:choose>                        
                        <div class="cell" role="cell"><a href="RequestServlet?ID_S=${tempRow.getID_Solicitud()}">${tempRow.getTotal()}</a></div>
                        <div class="cell" role="cell"><a href="RequestServlet?ID_S=${tempRow.getID_Solicitud()}">${tempRow.getFecha_Inicio()}</a></div>
                    </div>
                    </c:forEach>                    
                </div>
                <article class="Wrap_Details">
                    <div class="List_Request" role="table">
                        <div class="header_Request row" role="rowgroup">
                            <div class="cell" role="columnheader"><a href="#">Product Name</a></div>
                            <div class="cell" role="columnheader"><a href="#">Quantity</a></div>
                            <div class="cell" role="columnheader"><a href="#">Price</a></div>
                            <div class="cell" role="columnheader"><a href="#">Total Price</a></div>
                        </div>
                        <c:forEach var="tempCell" items="${ReqProductos}">
                        	<div class="row" role="rowgroup">
	                            <div class="cell" role="cell"><a href="#">${tempCell.getNombre()}</a></div>
	                            <div class="cell" role="cell"><a href="#">${tempCell.getCantidad()}</a></div>
	                            <div class="cell" role="cell"><a href="#">${tempCell.getPrecioSu()}</a></div>
	                            <div class="cell" role="cell"><a href="#">${tempCell.getTotal()}</a></div>
                       		</div>
                        </c:forEach>
                    </div>
                    <label class="Total_Price">Total Price: <label>${precioTotal}</label></label>
                    
                    	<div class="btn_Dec">
                    	<c:if test="${precioTotal>0 }">
	                       <a href ="RequestServlet?IDRequestAccept=${ID_Solicitud}" class="btn_Accept">Accept Request</a>
                       </c:if>
	                       <a href ="RequestServlet?IDRequestDecline=${ID_Solicitud}" class="btn_Decline">Decline Request</a>
                    	</div>
                    
                </article>
            </article>
            <article class="Wrap_Request">
                <h3 id="Declined_Req">Declined Request</h3>
                <div class="List_Request" role="table">
                    <div style="background:#2c2727;" class="header_Request row" role="rowgroup">
                        <div class="cell" role="columnheader"><a href="#">Request Num</a></div>
                        <c:choose>
                        	<c:when test="${UserInfo.getNivel()==0}">
                        		<div class="cell" role="columnheader"><a href="#">Customer NickName</a></div>
                        	</c:when>
                        	<c:otherwise>
                        		<div class="cell" role="columnheader"><a href="#">Quantity</a></div>
                        	</c:otherwise>
                        </c:choose>
                        <div class="cell" role="columnheader"><a href="#">Total</a></div>
                        <div class="cell" role="columnheader"><a href="#">Sent Date</a></div>
                    </div>
                    <c:forEach var="tempRow" items="${rechazados}">
                    <div class="row" role="rowgroup">
                        <div class="cell" role="cell"><a href="RequestServlet?ID_R=${tempRow.getID_Solicitud()}">${tempRow.getID_Solicitud()}</a></div>
                         <c:choose>
                        	<c:when test="${UserInfo.getNivel()==0}">
                        		<div class="cell" role="cell"><a href="RequestServlet?ID_R=${tempRow.getID_Solicitud()}">${tempRow.getNick()}</a></div>
                        	</c:when>
                        	<c:otherwise>
                        		<div class="cell" role="cell"><a href="RequestServlet?ID_R=${tempRow.getID_Solicitud()}">${tempRow.getCantidadProductos()}</a></div>
                        	</c:otherwise>
                        </c:choose> 
                        <div class="cell" role="cell"><a href="RequestServlet?ID_R=${tempRow.getID_Solicitud()}">${tempRow.getTotal()}</a></div>
                        <div class="cell" role="cell"><a href="RequestServlet?ID_R=${tempRow.getID_Solicitud()}">${tempRow.getFecha_Inicio()}</a></div>
                    </div>
                    </c:forEach>
                </div>
                <article class="Wrap_Details">
                    <div class="List_Request" role="table">
                     <div class="header_Request row" role="rowgroup">
                            <div class="cell" role="columnheader"><a href="#">Product Name</a></div>
                            <div class="cell" role="columnheader"><a href="#">Quantity</a></div>
                            <div class="cell" role="columnheader"><a href="#">Price</a></div>
                            <div class="cell" role="columnheader"><a href="#">Total Price</a></div>
                        </div>
                      <c:forEach var="tempCell" items="${DelProductos}">
                       	<div class="row" role="rowgroup">
                            <div class="cell" role="cell"><a href="#">${tempCell.getNombre()}</a></div>
                            <div class="cell" role="cell"><a href="#">${tempCell.getCantidad()}</a></div>
                            <div class="cell" role="cell"><a href="#">${tempCell.getPrecioSu()}</a></div>
                            <div class="cell" role="cell"><a href="#">${tempCell.getTotal()}</a></div>
                    	</div>
                      </c:forEach>                        
                    </div>
                    <label class="Total_Price">Total Price: <label>${precioTotalDel}</label></label>
                   
                </article>
            </article>
            <article class="Wrap_Request">
                <h3 id="Approved_Req">Approved Request</h3>
                <div class="List_Request" role="table">
                    <div style="background:#2c2727;" class="header_Request row" role="rowgroup">
                        <div class="cell" role="columnheader"><a href="#">Request Num</a></div>
                        <c:choose>
                        	<c:when test="${UserInfo.getNivel()==0}">
                        		<div class="cell" role="columnheader"><a href="#">Customer NickName</a></div>
                        	</c:when>
                        	<c:otherwise>
                        		<div class="cell" role="columnheader"><a href="#">Quantity</a></div>
                        	</c:otherwise>
                        </c:choose>
                        <div class="cell" role="columnheader"><a href="#">Total</a></div>
                        <div class="cell" role="columnheader"><a href="#">Sent Date</a></div>
                    </div>
                     <c:forEach var="tempRow" items="${aprobados}">
                    <div class="row" role="rowgroup">
                        <div class="cell" role="cell"><a href="RequestServlet?ID_A=${tempRow.getID_Solicitud()}">${tempRow.getID_Solicitud()}</a></div>
                         <c:choose>
                        	<c:when test="${UserInfo.getNivel()==0}">
                        		<div class="cell" role="cell"><a href="RequestServlet?ID_A=${tempRow.getID_Solicitud()}">${tempRow.getNick()}</a></div>
                        	</c:when>
                        	<c:otherwise>
                        		<div class="cell" role="cell"><a href="RequestServlet?ID_A=${tempRow.getID_Solicitud()}">${tempRow.getCantidadProductos()}</a></div>
                        	</c:otherwise>
                        </c:choose> 
                        <div class="cell" role="cell"><a href="RequestServlet?ID_A=${tempRow.getID_Solicitud()}">${tempRow.getTotal()}</a></div>
                        <div class="cell" role="cell"><a href="RequestServlet?ID_A=${tempRow.getID_Solicitud()}">${tempRow.getFecha_Inicio()}</a></div>
                    </div>
                    </c:forEach>                  
                </div>
                <article class="Wrap_Details">
                    <div class="List_Request" role="table">
                        <div class="header_Request row" role="rowgroup">
                            <div class="cell" role="columnheader"><a href="#">Product Name</a></div>
                            <div class="cell" role="columnheader"><a href="#">Quantity</a></div>
                            <div class="cell" role="columnheader"><a href="#">Price</a></div>
                            <div class="cell" role="columnheader"><a href="#">Total Price</a></div>
                        </div>
                      <c:forEach var="tempCell" items="${AppProductos}">
                       	<div class="row" role="rowgroup">
                            <div class="cell" role="cell"><a href="#">${tempCell.getNombre()}</a></div>
                            <div class="cell" role="cell"><a href="#">${tempCell.getCantidad()}</a></div>
                            <div class="cell" role="cell"><a href="#">${tempCell.getPrecioSu()}</a></div>
                            <div class="cell" role="cell"><a href="#">${tempCell.getTotal()}</a></div>
                    	</div>
                      </c:forEach>                          
                    </div>
                    <label class="Total_Price">Total Price: <label>${precioTotalApp}</label></label>
                  	<div class="btn_Dec">
                        <a id="Show_CheckOut" href ="#" class="btn_Accept">Accept Request</a>
                        <a href ="#" class="btn_Decline">Decline Request</a>
                    </div>
                  
                    <article class="Wrap_Checkout" id="Wrapper_CheckOut">
                       <form class="Form_Billing" action="RequestServlet" method="POST">
                            <section class="Wrap_Billing">
                            <input type="hidden" name="IDSOLI" value="${ID_Solicitud}">
                                <label for="ID_Name">Name</label>
                                <input id="ID_Name"type="text">

                                <label for="ID_Ape">Last Name</label>
                                <input id="ID_Ape"type="text">

                                <label for="ID_Email">Email</label>
                                <input id="ID_Email"type="text">

                                <label for="ID_Street">Street</label>
                                <input id="ID_Street"type="text">

                                <label for="ID_IntNumber">Interior Number</label>
                                <input id="ID_IntNumber"type="text">

                                <label for="ID_CP">CP</label>
                                <input id="ID_CP"type="text">
                            </section>
                            <section class="Wrap_Payment">
                                <label for="ID_Cash">Cash</label>
                                <input id="ID_Cash" type="radio" name="pago" value="Cash" checked>
                                <div class="Cash">
                                    Cash Deposit: 8597-52642-23145 BankName
                                </div>
                                <label for="ID_Card">Card</label>
                                <input id="ID_Card" type="radio" name="pago" value="Card">
                                <div class="Card">
                                    <label for="ID_Card_Name">Name on Card</label>
                                    <input id="ID_Card_Name"type="text">

                                    <label for="ID_Card_Number">Credit card number</label>
                                    <input id="ID_Card_Number"type="text">

                                    <label for="ID_Card_Exp">Exp Date</label>
                                    <input id="ID_Card_Exp"type="text">

                                    <label for="ID_Card_CVC">CVC</label>
                                    <input id="ID_Card_CVC"type="text">

                                   
                                </div>
                                
                            </section>
                            <input type="submit" value="Checkout">
                       </form>
                    </article>
                </article>
            </article>
        </section>
    </section>
	
	
</body>
</html>