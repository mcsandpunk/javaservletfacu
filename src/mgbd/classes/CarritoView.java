package mgbd.classes;

public class CarritoView {

	private int ID_Carrito;
	private int ID_Producto;
	private String Imagen;
	private String Articulo;
	private int Cantidad;
	private int Precio;
	
	public CarritoView() {
		
	}

	public int getID_Carrito() {
		return ID_Carrito;
	}

	public void setID_Carrito(int iD_Carrito) {
		ID_Carrito = iD_Carrito;
	}

	public int getID_Producto() {
		return ID_Producto;
	}

	public void setID_Producto(int iD_Producto) {
		ID_Producto = iD_Producto;
	}

	public String getImagen() {
		return Imagen;
	}

	public void setImagen(String imagen) {
		Imagen = imagen;
	}

	public String getArticulo() {
		return Articulo;
	}

	public void setArticulo(String articulo) {
		Articulo = articulo;
	}

	public int getCantidad() {
		return Cantidad;
	}

	public void setCantidad(int cantidad) {
		Cantidad = cantidad;
	}

	public int getPrecio() {
		return Precio;
	}

	public void setPrecio(int precio) {
		Precio = precio;
	}
	
	
}
