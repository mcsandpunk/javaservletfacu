package mgbd.classes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FuncionesVarias {

	public static boolean ValidarEmail(String email) {
		
		/*Pattern pat = Pattern.compile("([a-z0-9]+(\\.?[a-z0-9])*)+@(([a-z]+)\\.([a-z]+))+");
		
		 Matcher mather = pat.matcher(email);
		 
		 if (mather.find() == true) {
	            return true;
	        } else {
	            return false;
	        }*/
		String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
	      return email.matches(regex);
	}
	
	public static boolean ValidarNumeros(String cadena) {
		
		 try {
	            Integer.parseInt(cadena);
	            return true;
	        } catch (NumberFormatException excepcion) {
	            return false;
	        }
	}
	
	public static boolean ValidarNumDouble(String cadena) {
		try {
			Double.parseDouble(cadena);
			return true;
		}catch(NumberFormatException excepcion) {
			return false;
		}
	}
	
	public static boolean ValidarLongitud(String cadena,int cantidad) {
		
		if(cadena.trim().length()<cantidad) {
			return false;
		}
		else
			return true;
	}
	
	public static boolean isVideo(String nombrearchivo) {
		
		String nombre= nombrearchivo;
		String sbr = nombre.substring(nombre.length()-3,nombre.length());
		
		if(sbr.equals("mp4")||sbr.equals("ebm"))
			return true;
		else
			return false;
	}
	
	public FuncionesVarias() {
		
	}
}
