package mgbd.classes;

import java.util.Date;

public class Product {

	private int ID;
	private String Articulo;
	private String Descripcion;
	private int Unidad;
	private boolean Estado;
	private int vistas;
	private Date Fecha_Creacion;
	private boolean Publicado;
	private String direccionImagen;
	
	private int Rating;
	private String sCategoria;
	private int iCategoria;
	private String sPart;
	private int iPart;
	
	public double precio;
	
	
	
	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		System.out.println("Entro a Set Precio");
		this.precio = precio;
	}

	public String getsCategoria() {
		return sCategoria;
	}

	public void setsCategoria(String sCategoria) {
		this.sCategoria = sCategoria;
	}

	public int getiCategoria() {
		return iCategoria;
	}

	public void setiCategoria(int iCategoria) {
		this.iCategoria = iCategoria;
	}

	public String getsPart() {
		return sPart;
	}

	public void setsPart(String sPart) {
		this.sPart = sPart;
	}

	public int getiPart() {
		return iPart;
	}

	public void setiPart(int iPart) {
		this.iPart = iPart;
	}

	public int getRating() {
		return Rating;
	}

	public void setRating(int rating) {
		Rating = rating;
	}

	public boolean isPublicado() {
		return Publicado;
	}

	public void setPublicado(boolean publicado) {
		Publicado = publicado;
	}

	public Product() {
		
	}
	
	public String getDireccionImagen() {
		return direccionImagen;
	}




	public void setDireccionImagen(String direccionImagen) {
		this.direccionImagen = direccionImagen;
	}




	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getArticulo() {
		return Articulo;
	}

	public void setArticulo(String articulo) {
		Articulo = articulo;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	public int getUnidad() {
		return Unidad;
	}

	public void setUnidad(int unidad) {
		Unidad = unidad;
	}

	public boolean isEstado() {
		return Estado;
	}

	public void setEstado(boolean estado) {
		Estado = estado;
	}

	public int getVistas() {
		return vistas;
	}

	public void setVistas(int vistas) {
		this.vistas = vistas;
	}

	public Date getFecha_Creacion() {
		return Fecha_Creacion;
	}

	public void setFecha_Creacion(Date fecha_Creacion) {
		Fecha_Creacion = fecha_Creacion;
	}
	
	
}
