package mgbd.classes;

public class Parts {
	
	
	
	public Parts(int iD_Parts, String part, int estado) {
		
		ID_Parts = iD_Parts;
		Part = part;
		Estado = estado;
	}
	
	
	public Parts(String part, int estado) {
		
		Part = part;
		Estado = estado;
	}
	
	


	public int getID_Parts() {
		return ID_Parts;
	}


	public void setID_Parts(int iD_Parts) {
		ID_Parts = iD_Parts;
	}


	public String getPart() {
		return Part;
	}


	public void setPart(String part) {
		Part = part;
	}


	public int getEstado() {
		return Estado;
	}


	public void setEstado(int estado) {
		Estado = estado;
	}

	@Override
	public String toString() {
		return "Parts [ID_Parts=" + ID_Parts + ", Part=" + Part + ", Estado=" + Estado + "]";
	}




	private int ID_Parts;
	private String Part;
	private int Estado;
}
