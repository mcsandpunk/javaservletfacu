package mgbd.classes;

import java.util.Date;

public class ViewRequest {

	private String Estado;
	private int ID_Solicitud;
	private int ID_User;
	private String Nick;
	private double Total;
	private Date Fecha_Inicio;
	private int CantidadProductos;
	
	private String Compra;
	
	
	
	public String getCompra() {
		return Compra;
	}

	public void setCompra(String compra) {
		Compra = compra;
	}

	public int getCantidadProductos() {
		return CantidadProductos;
	}

	public void setCantidadProductos(int cantidadProductos) {
		CantidadProductos = cantidadProductos;
	}

	public ViewRequest() {
		
	}

	public String getEstado() {
		return Estado;
	}

	public void setEstado(String estado) {
		Estado = estado;
	}

	public int getID_Solicitud() {
		return ID_Solicitud;
	}

	public void setID_Solicitud(int iD_Solicitud) {
		ID_Solicitud = iD_Solicitud;
	}

	public int getID_User() {
		return ID_User;
	}

	public void setID_User(int iD_User) {
		ID_User = iD_User;
	}

	public String getNick() {
		return Nick;
	}

	public void setNick(String nick) {
		Nick = nick;
	}

	public double getTotal() {
		return Total;
	}

	public void setTotal(double total) {
		Total = total;
	}

	public Date getFecha_Inicio() {
		return Fecha_Inicio;
	}

	public void setFecha_Inicio(Date fecha_Inicio) {
		Fecha_Inicio = fecha_Inicio;
	}
	
	
}
