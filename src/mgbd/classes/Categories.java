package mgbd.classes;

public class Categories {

	private int ID;
	private String Catego;
	private boolean Estado;
	
	public Categories() {
		
	}
	
	public Categories(int iD, String catego, boolean estado) {
		ID = iD;
		Catego = catego;
		Estado = estado;
	}


	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getCatego() {
		return Catego;
	}

	public void setCatego(String catego) {
		Catego = catego;
	}

	public boolean isEstado() {
		return Estado;
	}

	public void setEstado(boolean estado) {
		Estado = estado;
	}
	
	
}
