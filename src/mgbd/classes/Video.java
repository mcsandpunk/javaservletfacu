package mgbd.classes;

public class Video {

	private int iD;
	private String Direccion;
	private int FK_Producto;
	
	public Video() {
		
	}

	public int getiD() {
		return iD;
	}

	public void setiD(int iD) {
		this.iD = iD;
	}

	public String getDireccion() {
		return Direccion;
	}

	public void setDireccion(String direccion) {
		Direccion = direccion;
	}

	public int getFK_Producto() {
		return FK_Producto;
	}

	public void setFK_Producto(int fK_Producto) {
		FK_Producto = fK_Producto;
	}
	
	
}
