package mgbd.classes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Properties;


import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.UUID;

public class Mailer {

	public static void send(String to) {
		
		final String user = "Edgar.Fuentes.Rodriguez@hotmail.com";
		final String pass = "15Edgar18EsLMAD26";
		
		//Obtener el objeto de Sesion Actual
		
		Properties props = new Properties();
		
		/*props.put("mail.smtp.host","smtp.live.com");
		props.put("mail.smtp.auth","true");*/
		
		   props.setProperty("mail.transport.protocol", "smtp");
		    props.setProperty("mail.host", "smtp.live.com");
		    props.put("mail.smtp.starttls.enable", "true");
		    props.put("mail.smtp.auth", "true");
		    props.put("mail.smtp.port", "587");
		
		Session session = Session.getDefaultInstance(props,  
				 new javax.mail.Authenticator() {  
				  protected PasswordAuthentication getPasswordAuthentication() {  
				   return new PasswordAuthentication(user,pass);  
				   }  
				});   
				
		
		//Crear Mensaje
		
		try {
			
			BufferedReader br = null;
	        FileReader fr = null;

	            //br = new BufferedReader(new FileReader(FILENAME));
	            fr = new FileReader("C:/Users/Edgar/Desktop/Proyecto_PAPW_BDM_PM_DESIGN/message.html");
	            br = new BufferedReader(fr);

	            String sCurrentLine;
	            String outCome="";

	            while ((sCurrentLine = br.readLine()) != null) {
	                System.out.println(sCurrentLine);
	                outCome+="\n"+sCurrentLine;
	            }
	            
	            br.close();
	            
           MimeMultipart multipart = new MimeMultipart("related");
           
           MimeBodyPart messageBodyPart = new MimeBodyPart();
           messageBodyPart.setText(outCome,"US-ASCII", "html");
           
           /*String htmlText = "<H1>Hello</H1><img src=\"cid:image\">";
           messageBodyPart.setContent(htmlText, "text/html");*/
           
           multipart.addBodyPart(messageBodyPart);
           
           messageBodyPart = new MimeBodyPart();
           //DataSource fds = new FileDataSource("C:\\Users\\Edgar\\Desktop\\Avatars - xmas giveaway _)\\PNG\\512\\watch.png");
           messageBodyPart.attachFile("C:\\Users\\Edgar\\Desktop\\Proyecto_PAPW_BDM_PM_DESIGN\\icono\\Logo.png");
           //messageBodyPart.setDataHandler(new DataHandler(fds));
           messageBodyPart.setHeader("Content-ID", "<image>");
           messageBodyPart.setDisposition(MimeBodyPart.INLINE);
           
           multipart.addBodyPart(messageBodyPart);
           
          /* messageBodyPart = new MimeBodyPart();
           messageBodyPart.attachFile("C:\\Users\\Edgar\\Desktop\\Avatars - xmas giveaway _)\\PNG\\512\\watch.png");
           messageBodyPart.setHeader("Content-ID", "<image2>");
           messageBodyPart.setDisposition(MimeBodyPart.INLINE);
           
           multipart.addBodyPart(messageBodyPart);*/
           
           /******************************************/
			MimeMessage message = new MimeMessage(session);
			
			message.setFrom(new InternetAddress(user));
			
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			
			message.setSubject("Thank you for Register on MGBD_Parts");
			message.setContent( multipart);
			Transport.send(message);
			
			System.out.println("Listo");
			
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
		
	}
	
	String generateContentId(String prefix) {
	     return String.format("%s-%s", prefix, UUID.randomUUID());
	}
		
}
