package mgbd.classes;

import java.util.Date;

public class Solicitud {

	private int ID;
	private Date Fecha_Inicio;
	private Date Fecha_Cierre;
	private String Estado;
	private String Remision;
	private String Compra;
	private String Pago;
	private String Envio;
	
	public Solicitud() {
		
	}
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public Date getFecha_Inicio() {
		return Fecha_Inicio;
	}
	public void setFecha_Inicio(Date fecha_Inicio) {
		Fecha_Inicio = fecha_Inicio;
	}
	public Date getFecha_Cierre() {
		return Fecha_Cierre;
	}
	public void setFecha_Cierre(Date fecha_Cierre) {
		Fecha_Cierre = fecha_Cierre;
	}
	public String getEstado() {
		return Estado;
	}
	public void setEstado(String estado) {
		Estado = estado;
	}
	public String getRemision() {
		return Remision;
	}
	public void setRemision(String remision) {
		Remision = remision;
	}
	public String getCompra() {
		return Compra;
	}
	public void setCompra(String compra) {
		Compra = compra;
	}
	public String getPago() {
		return Pago;
	}
	public void setPago(String pago) {
		Pago = pago;
	}
	public String getEnvio() {
		return Envio;
	}
	public void setEnvio(String envio) {
		Envio = envio;
	}
	
	
}
