package mgbd.classes;

public class Carrito {

	private int ID;
	private int Cantidad;
	private int FK_P;
	private int FK_U;
	private int FK_S;
	private String Estado;
	private double PrecioSu;
	private double Total;
	
	private String nombre;
	
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getTotal() {
		return Total;
	}
	public void setTotal(double total) {
		Total = total;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public int getCantidad() {
		return Cantidad;
	}
	public void setCantidad(int cantidad) {
		Cantidad = cantidad;
	}
	public int getFK_P() {
		return FK_P;
	}
	public void setFK_P(int fK_P) {
		FK_P = fK_P;
	}
	public int getFK_U() {
		return FK_U;
	}
	public void setFK_U(int fK_U) {
		FK_U = fK_U;
	}
	public int getFK_S() {
		return FK_S;
	}
	public void setFK_S(int fK_S) {
		FK_S = fK_S;
	}
	public String getEstado() {
		return Estado;
	}
	public void setEstado(String estado) {
		Estado = estado;
	}
	public double getPrecioSu() {
		return PrecioSu;
	}
	public void setPrecioSu(double precioSu) {
		PrecioSu = precioSu;
	}
	
	public Carrito() {
		
	}
}
