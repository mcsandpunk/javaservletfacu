package mgbd.classes;

import java.io.InputStream;
import java.util.Date;

public class User {

	private int ID;
	private byte[] imagenByte;
	private InputStream imagen;
	private String nombre;
	private String apellido;
	private String email;
	private String nickname;
	private short nivel;
	private Date fecha_Creacion;
	private boolean Estado;
	
	public User() {
		
	}
	
	public User(int iD, byte[] imagenByte, InputStream imagen, String nombre, String apellido, String email,
			String nickname, short nivel, Date fecha_Creacion, boolean estado) {
		super();
		ID = iD;
		this.imagenByte = imagenByte;
		this.imagen = imagen;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.nickname = nickname;
		this.nivel = nivel;
		this.fecha_Creacion = fecha_Creacion;
		Estado = estado;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public byte[] getImagenByte() {
		return imagenByte;
	}

	public void setImagenByte(byte[] imagenByte) {
		this.imagenByte = imagenByte;
	}

	public InputStream getImagen() {
		return imagen;
	}

	public void setImagen(InputStream imagen) {
		this.imagen = imagen;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public short getNivel() {
		return nivel;
	}

	public void setNivel(short nivel) {
		this.nivel = nivel;
	}

	public Date getFecha_Creacion() {
		return fecha_Creacion;
	}

	public void setFecha_Creacion(Date fecha_Creacion) {
		this.fecha_Creacion = fecha_Creacion;
	}

	public boolean isEstado() {
		return Estado;
	}

	public void setEstado(boolean estado) {
		Estado = estado;
	}
	
	
	
	
}
