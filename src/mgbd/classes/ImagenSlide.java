package mgbd.classes;

import java.io.InputStream;

public class ImagenSlide {
	
	private int ID;
	private InputStream imagen;
	private byte[] bytesImagen;
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public InputStream getImagen() {
		return imagen;
	}
	public void setImagen(InputStream imagen) {
		this.imagen = imagen;
	}
	public byte[] getBytesImagen() {
		return bytesImagen;
	}
	public void setBytesImagen(byte[] bytesImagen) {
		this.bytesImagen = bytesImagen;
	}
	public ImagenSlide(int iD, InputStream imagen, byte[] bytesImagen) {
		
		ID = iD;
		this.imagen = imagen;
		this.bytesImagen = bytesImagen;
	}
	
	public ImagenSlide(){
		
	}
	
	

}
