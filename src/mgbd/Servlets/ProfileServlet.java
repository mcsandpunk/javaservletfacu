package mgbd.Servlets;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.sql.DataSource;

import mgbd.Modales.*;
import mgbd.classes.*;

/**
 * Servlet implementation class ProfileServlet
 */
@WebServlet("/ProfileServlet")
@MultipartConfig()
public class ProfileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="Pool_Connection_BDM")
	
	private DataSource miPool;
	
	private Cookie[] AllCookies;
	private ModeloUser modeloUsuario;
	private ModeloImagenSlide modeloSlide;
	private ModeloCarrito modeloCarrito;
	private ModeloParts modeloParts;
	private User usuario;
       
    @Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		modeloUsuario = new ModeloUser(miPool);
		modeloSlide = new ModeloImagenSlide(miPool);
		modeloCarrito = new ModeloCarrito(miPool);
		modeloParts = new ModeloParts(miPool);
	}

	/**
     * @see HttpServlet#HttpServlet()
     */
    public ProfileServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		AllCookies = request.getCookies();
		String ID="0";
		if(AllCookies!=null) {
			for(Cookie temp_Cookie:AllCookies) {
				if("MGBD.Parts.UserADMINID".equals(temp_Cookie.getName())) {
					ID = temp_Cookie.getValue();
					System.out.println("Encontro la cookie");
				}
				else if("MGBD.Parts.UserID".equals(temp_Cookie.getName())) {
					ID = temp_Cookie.getValue();
					System.out.println("Encontro la cookie");
				}	
			}
		}
		
		if(!ID.equals("0")) {
			usuario = modeloUsuario.getUser(Integer.parseInt(ID));
			System.out.println("Se agrega Usuario");
			request.setAttribute("UserInfo",usuario );
			
			System.out.println("Estado GET: "+usuario.isEstado());
			
			List<ImagenSlide> imagenesSlide= modeloSlide.obtenerImagenes();
			
			request.setAttribute("Imagenes", imagenesSlide);
			
			int todosProductos = modeloCarrito.getAllCarrito(usuario.getID());
			
			System.out.println("Cantidad de articulos del Usuario: "+ todosProductos);
			
			request.setAttribute("productos", todosProductos);
			
			List<Parts> ListaParts;
			try {
				System.out.println("Obtiene Parts");
				ListaParts=modeloParts.getParts();
				request.setAttribute("ListaParts",ListaParts);
				
			}catch(Exception e) {
				
				e.printStackTrace();
			}
			
			RequestDispatcher miDispatcher = request.getRequestDispatcher("/Settings.jsp");			
			miDispatcher.forward(request, response);
		}
		else {
			System.out.println("A Index de nuevo");
			response.sendRedirect(request.getContextPath()+"/IndexServlet");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		
		updateSlide(request, response);
		decidirForm(request,response);

	}
	
	private void decidirForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String Form = request.getParameter("Send");
		System.out.println("FORM: " + Form);
		
		if(Form!=null) {
			if(Form.equals("Update Info")) {
				
				if(verificarDatos(request,response)) {				
					updateSettings(usuario,request,response);
				}
			}
		}
	}
	
	private boolean verificarDatos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name = request.getParameter("Name_User");
		String apellido = request.getParameter("Last_Name");
		String email = request.getParameter("Email");
		String nick = request.getParameter("NickName");
		String pass=request.getParameter("pass");
		String repass = request.getParameter("repass");
		
		if(!FuncionesVarias.ValidarLongitud(name, 5)) {
			System.out.println("NoName");
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("NoName");
			return false;
		}
		
		if(!FuncionesVarias.ValidarLongitud(apellido, 5)) {
			System.out.println("NoApe");
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("NoApe");
			return false;
		}
		
		if(!FuncionesVarias.ValidarEmail(email)) {
			System.out.println("NoEmail");
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("NoEmail");
			return false;
		}
		
		if(!FuncionesVarias.ValidarLongitud(nick, 5)) {
			System.out.println("NoNick");
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("NoNick");
			return false;
		}
		System.out.println("Nick: "+nick);
		if(pass!=null) {
			System.out.println("Comprueba Pass: "+pass.length());
			if(pass.length()>0) {
				if(!FuncionesVarias.ValidarLongitud(pass, 4)) {
					System.out.println("NoPass");
					response.setContentType("text/plain");
					response.setCharacterEncoding("UTF-8");
					response.getWriter().write("NoPass");
					return false;
				}
				
				if(!pass.equals(repass)) {
					System.out.println("NoRePass");
					response.setContentType("text/plain");
					response.setCharacterEncoding("UTF-8");
					response.getWriter().write("NoRePass");
					return false;
				}
			}
		}
		
		
		return true;
		
		
	}
	
	private void updateSettings(User usuario, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name = request.getParameter("Name_User");
		String apellido = request.getParameter("Last_Name");
		String email = request.getParameter("Email");
		String nick = request.getParameter("NickName");
		String pass=request.getParameter("pass");
		String repass = request.getParameter("repass");
		
		usuario.setNombre(name);
		usuario.setApellido(apellido);
		usuario.setEmail(email);
		usuario.setNickname(nick);
		
		Part part = null;
		try {
			part = request.getPart("ImageUser");
			System.out.println("PAAAAAAAART");
		}catch(IllegalStateException e) {
			System.out.println("NoImage");
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("NoImage");
			return;
		}
		System.out.println(part);
		if(part!=null) {
			System.out.println("PART DIFERENTE A NULL");
			InputStream inputStream = null;
			if(part.getSize() >0) {
				inputStream = part.getInputStream();
				if(inputStream != null) {
					usuario.setImagen(inputStream);
				}
			}
		}
	
		System.out.println("Estado antes de UPDATE: "+usuario.isEstado());
		if(pass!=null) {
			if(pass.length()>0) {
				System.out.println("Pass mayor");
				modeloUsuario.updateUsuario(usuario, pass);
				response.sendRedirect(request.getContextPath()+"/ProfileServlet");
			}
			else {
				System.out.println("Pass menos null");
				modeloUsuario.updateUsuario(usuario, null);
				response.sendRedirect(request.getContextPath()+"/ProfileServlet");
			}
		}
	}
	
	private void updateSlide(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String subir = request.getParameter("btn_send");
		ImagenSlide imagen = new ImagenSlide();
		if(subir!=null) {
			
			Part part = null;
			try {
				part = request.getPart("ImageSlide");
				
			}catch(IllegalStateException e) {
				System.out.println("NoImage");
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write("NoImage");
				return;
			}
			System.out.println(part);
			if(part!=null) {
				
				InputStream inputStream = null;
				if(part.getSize() >0) {
					inputStream = part.getInputStream();
					if(inputStream != null) {
						imagen.setImagen(inputStream);
						modeloSlide.insertImageSlide(imagen);
						response.sendRedirect(request.getContextPath()+"/ProfileServlet");
					
					}
				}
			}
			
		}
		
		String IDEliminar = request.getParameter("deleteID");
		
		if(IDEliminar!=null) {
			int cantidad = modeloSlide.getAllImagesSlide();
			if(cantidad<=2) {
				System.out.println("NoPass");
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write("NoDelete");
				return;
			}else {
				modeloSlide.deleteImagenSlide(Integer.parseInt(IDEliminar));
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write("Deleted");		
				
				return;
			}
		}
	}

}
