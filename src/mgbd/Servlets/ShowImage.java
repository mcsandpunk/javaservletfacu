package mgbd.Servlets;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ShowImage
 */
@WebServlet("/ShowImage/*")
public class ShowImage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
	private static final String SAVE_DIR = "uploadFiles";
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowImage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		System.out.println("Entro a ShowImage");
		String nombreImagen = request.getPathInfo().substring(1);
		
		
		String appPath = request.getServletContext().getRealPath("/");
		String RutaCompleta = appPath+File.separator+ SAVE_DIR + nombreImagen;
		
		System.out.println("Se supone que lo encuentro en: "+RutaCompleta);
		
		File file = new File(appPath+File.separator+SAVE_DIR,nombreImagen);
		response.setHeader("Content-type",getServletContext().getMimeType(nombreImagen));
		response.setHeader("Content-Length", String.valueOf(file.length()));
		response.setHeader("Content-Disposition", "inline;filename=\""+nombreImagen+"\"");
		Files.copy(file.toPath(),response.getOutputStream());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
