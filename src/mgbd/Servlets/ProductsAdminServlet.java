package mgbd.Servlets;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.sql.DataSource;

import mgbd.Modales.*;
import mgbd.classes.*;

/**
 * Servlet implementation class ProductsAdminServlet
 */
@WebServlet("/ProductsAdminServlet")
@MultipartConfig(maxFileSize = 1024*1024*5)
public class ProductsAdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	private static final String SAVE_DIR = "uploadFiles";
	@Resource(name="Pool_Connection_BDM")
	
	private DataSource miPool;
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	private Cookie[] AllCookies; 
	private ModeloUser modeloUsuario;
	private ModeloParts modeloParts;
	private ModeloCategories modeloCatego;
	private ModeloProduct modeloProduct;
	private ModeloImageProduct modeloImagenProduct;
	private ModeloVideo	modeloVideo;
	
    public ProductsAdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		
		modeloUsuario = new ModeloUser(miPool);
		modeloParts = new ModeloParts(miPool);
		modeloCatego = new ModeloCategories(miPool);
		modeloProduct = new ModeloProduct(miPool);
		modeloImagenProduct = new ModeloImageProduct(miPool);
		modeloVideo = new ModeloVideo(miPool);
		
	}

	 protected void processRequest(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	        response.setContentType("text/html;charset=UTF-8");
	        Collection<Part> parts = request.getParts();
	        for(Part part : parts) {
	                part.write(getFileName(part));
	        }
	    }
	 
	 public String getFileName(Part part) {
	        String contentHeader = part.getHeader("content-disposition");
	        String[] subHeaders = contentHeader.split(";");
	        for(String current : subHeaders) {
	            if(current.trim().startsWith("filename")) {
	                int pos = current.indexOf('=');
	                String fileName = current.substring(pos+1);
	                return fileName.replace("\"", "");
	            }
	        }
	        return null;
	    }


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	
		AllCookies = request.getCookies();
		String ID="0";
		if(AllCookies!=null) {
			for(Cookie temp_Cookie:AllCookies) {
				if("MGBD.Parts.UserADMINID".equals(temp_Cookie.getName())) {
					ID = temp_Cookie.getValue();
					System.out.println("Encontro la cookie");
				}
			}
		}
		
		User usuario;
		
		if(!ID.equals("0")) {
			usuario = modeloUsuario.getUser(Integer.parseInt(ID));
			System.out.println("Se agrega Usuario");
			request.setAttribute("UserInfo",usuario );		
			
			List<Parts> ListaParts;
			List<Categories> ListaCatego;
			List<Product> ListaProduct;
			try {
				
				ListaParts=modeloParts.getParts();
				request.setAttribute("ListaParts",ListaParts);
				
				ListaCatego = modeloCatego.getCategories();
				request.setAttribute("ListaCatego", ListaCatego);
				
				ListaProduct = modeloProduct.getAllAdminProduct();
				request.setAttribute("ListaProduct", ListaProduct);
				
			}catch(Exception e) {
				
				e.printStackTrace();
			}
			
			String ID_Delete = request.getParameter("DeleteImageID");
			if(ID_Delete!=null) {
				try {
					if(modeloImagenProduct.stillImages(Integer.parseInt(request.getParameter("IDProduct")))>1) {
						modeloImagenProduct.DeleteImageFromProduct(Integer.parseInt(ID_Delete));
					}
					else
						request.setAttribute("Aviso", "You can't delete the last Image");
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			String ID_VideoDelete = request.getParameter("DeleteVideoID");
			if(ID_VideoDelete!=null) {
				modeloVideo.DeleteVideoProducto(Integer.parseInt(ID_VideoDelete));
			}
			String ID_Producto = request.getParameter("IDProduct");
			if(ID_Producto!=null) {
				Product producto;
				try {
					producto = modeloProduct.getProduct(Integer.parseInt(ID_Producto));
					List<ImageProduct> imagenes=modeloImagenProduct.getAllImagesProduct(producto.getID());
					
					List<Video> videos = modeloVideo.getAllVideoFromProducto(producto.getID());
					
					request.setAttribute("VideosPro", videos);
					request.setAttribute("productoGET", producto);
					request.setAttribute("ImagenesPro", imagenes);
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			
			
			RequestDispatcher miDispatcher = request.getRequestDispatcher("admin_product.jsp");
			
			miDispatcher.forward(request, response);
		}
		else {
			System.out.println("A Index de nuevo");
			response.sendRedirect(request.getContextPath()+"/IndexServlet");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		String form = request.getParameter("Form");
		System.out.println("Formulario: "+ form);
		if(form.equals("AddProduct")) {
			String name = request.getParameter("ProductName");
			String desc = request.getParameter("Descript");
			String quant = request.getParameter("QuanAvailable");
			String cate = request.getParameter("Category");
			String part = request.getParameter("Part");
			String active = request.getParameter("Active");
			String publish = request.getParameter("Publish");
			String price = request.getParameter("Price");
			
			System.out.println(name);
			System.out.println(desc);
			System.out.println(quant);
			System.out.println(cate);
			System.out.println(part);
			System.out.println(active);
			
			if(!FuncionesVarias.ValidarLongitud(name, 5)) {
				System.out.println("NombreCorto");
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write("NombCorto");
				return;
			}
			
			if(!FuncionesVarias.ValidarLongitud(desc, 10)) {
				System.out.println("NombreCorto");
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write("DescCorto");
				return;
			}
			
			if(!FuncionesVarias.ValidarNumeros(quant)) {
				
				System.out.println("No es un numero");
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write("NoNumber");
				return;
			}
			
			if(!FuncionesVarias.ValidarNumDouble(price)) {
				System.out.println("No es un numero");
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write("NoNumberPrice");
				return;
			}
			
			if(!checkPart("Product_Image",request,response)) {
				System.out.println("No hay videos ni Imagen");
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write("NoImageVideo");
				return;
			}
			
			modeloProduct.AddProduct(name, desc, Integer.parseInt(quant), (active==null)?false:true, Integer.parseInt(part), 0, Integer.parseInt(cate),Double.parseDouble(price));
			int ID = modeloProduct.getLastID();
			modeloProduct.setPublicado((publish==null)?false:true, ID);
			
			String appPath = request.getServletContext().getRealPath("/");
			
			String finalDocLocation = appPath+File.separator+SAVE_DIR;
			System.out.println("Se guarda en: "+finalDocLocation);
			File fileSaveDir = new File(finalDocLocation);
			if(!fileSaveDir.exists()) {
				fileSaveDir.mkdirs();
			}
			
			try {
				
				List<String> rutas = subirArchivo("Product_Image",ID,fileSaveDir,request,response);
				
				if(rutas.size()<=0) {
					System.out.println("No se escogio Imagen ni Video");
					response.setContentType("text/plain");
					response.setCharacterEncoding("UTF-8");
					response.getWriter().write("NoImageVideo");
					
					return;
				}
				
				for(String ruta:rutas) {
					if(FuncionesVarias.isVideo(ruta))
						modeloVideo.AddVideo(ID, ruta);
					else
						modeloImagenProduct.addImage(ruta, ID);
					
					System.out.println("AddImage");
				}
				System.out.println("Fin de Agregar");
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write("Nice");
			
				return;
			}catch(Exception e) {
				e.printStackTrace();
			}
			
		}
		else if(form.equals("Edit_Product")) {
			String IDProducto = request.getParameter("IDHidden");
			
			if(!VerificarDatos(Integer.parseInt(IDProducto),request,response))
				return;
			
			String name = request.getParameter("ProductName");
			String desc = request.getParameter("Descript");
			String quant = request.getParameter("QuanAvailable");
			String cate = request.getParameter("Category");
			String part = request.getParameter("Part");
			String active = request.getParameter("Active");
			String publish = request.getParameter("Publish");
			String price = request.getParameter("Price");
			
			modeloProduct.UpdateProduct(Integer.parseInt(IDProducto), name, desc,
					Integer.parseInt(quant), (active==null)?false:true, 
							Integer.parseInt(part), Integer.parseInt(cate), (publish==null)?false:true,Double.parseDouble(price));
			
			String appPath = request.getServletContext().getRealPath("/");
			
			String finalDocLocation = appPath+File.separator+SAVE_DIR;
			System.out.println("Se guarda en: "+finalDocLocation);
			File fileSaveDir = new File(finalDocLocation);
			if(!fileSaveDir.exists()) {
				fileSaveDir.mkdirs();
			}
			
			try {
				List<String> rutas = subirArchivo("Product_Image",Integer.parseInt(IDProducto),fileSaveDir,request,response);
				
				for(String ruta:rutas) {
					if(FuncionesVarias.isVideo(ruta))
						modeloVideo.AddVideo(Integer.parseInt(IDProducto), ruta);
					else
						modeloImagenProduct.addImage(ruta, Integer.parseInt(IDProducto));
					
					System.out.println("EditImage");
				}
				System.out.println("Fin de Editar");
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write("Nice");
			
				return;
			}catch(Exception e) {
				e.printStackTrace();
			}
			
		}
		
		
	}
	
	private boolean VerificarDatos(int ID_Producto,HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
		
		String name = request.getParameter("ProductName");
		String desc = request.getParameter("Descript");
		String quant = request.getParameter("QuanAvailable");
		String cate = request.getParameter("Category");
		String part = request.getParameter("Part");
		String active = request.getParameter("Active");
		String publish = request.getParameter("Publish");
		String price = request.getParameter("Price");
		
		System.out.println(name);
		System.out.println(desc);
		System.out.println(quant);
		System.out.println(cate);
		System.out.println(part);
		System.out.println(active);
		System.out.println(publish);
		
		if(!FuncionesVarias.ValidarLongitud(name, 5)) {
			System.out.println("NombreCortoEdit");
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("NombCorto");
			return false;
		}
		
		if(!FuncionesVarias.ValidarLongitud(desc, 10)) {
			System.out.println("NombreCorto");
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("DescCorto");
			return false;
		}
		
		if(!FuncionesVarias.ValidarNumeros(quant)) {
			
			System.out.println("No es un numero");
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("NoNumber");
			return false;
		}
		
		if(!FuncionesVarias.ValidarNumDouble(price)) {
			System.out.println("No es un numero");
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("NoNumberPrice");
			return false;
		}		
		
		if(modeloImagenProduct.stillImages(ID_Producto)==0) {
			if(!checkPart("Product_Image",request,response)) {
				System.out.println("No hay videos ni Imagen");
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write("NoImageVideo");
				return false;
			}		
		}
		
		return true;
	}
	
	private boolean checkPart(String nombrePart,HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
		
		for(Part part:request.getParts()){
			if(!part.getName().equals(nombrePart)) {
				continue;
			}
			String nombreArchivo = obtenerNombreArchivo(part);
			if(nombreArchivo.equals(""))
				return false;
		}
		
		return true;
	}
	
	
	private List<String> subirArchivo(String nombrePart,int ID,File uploadFilePath,HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
		List<String> archivos = new ArrayList<>();
		
		try {
			for(Part part:request.getParts()) {
				if(!part.getName().equals(nombrePart)){
					continue;
				}
				String nombreArchivo = obtenerNombreArchivo(part);
				if(nombreArchivo.equals(""))
					return archivos;
				File archivo = File.createTempFile("cod"+ID+"-","-"+nombreArchivo,uploadFilePath );
				 
				try (InputStream input=part.getInputStream()){
					Files.copy(input, archivo.toPath(),StandardCopyOption.REPLACE_EXISTING);
					System.out.println("Se termina guardando en: "+archivo.getPath());
					archivos.add(obtenerNombreArchivo(archivo.getPath()));
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		}catch(IllegalStateException e) {
			System.out.println("Demasiado Grandes los archivos");
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("SizeBig");
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return archivos;
	}
	
	private String obtenerNombreArchivo(Part part) {
		String nombre="";
		
		String contentHeader = part.getHeader("content-disposition");
		String[] tokens = contentHeader.split(";");
		
		for(String token:tokens) {
			System.out.println("TokenFirst: "+token);
			if(token.trim().startsWith("filename")) {
				System.out.println("Token: "+token);
				
				int pos = token.indexOf('=');
				String fileName = token.substring(pos+1);
				
				System.out.println("FileName: "+fileName);
				String archivo = fileName.replace("\"","");
				
				System.out.println("Nombre Archivo: "+archivo);
				
				return archivo;
			}
		}
		return nombre;
		
	}
	
	private String obtenerNombreArchivo(String ruta) {
		String nombre="";
		if(ruta.lastIndexOf("\\")<0) {
			nombre = ruta.substring(ruta.lastIndexOf('/'),ruta.length());
		}
		else {
			nombre = ruta.substring(ruta.lastIndexOf("\\")+1,ruta.length());
		}
		
		return nombre;
	}
}
