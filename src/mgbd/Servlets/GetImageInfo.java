package mgbd.Servlets;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;


import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import mgbd.classes.*;
import mgbd.Modales.*;

/**
 * Servlet implementation class GetImageInfo
 */
@WebServlet("/GetImageInfo")
public class GetImageInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="Pool_Connection_BDM")
	private DataSource miPool;
	
	ModeloImagenSlide imagenModelo;
	ModeloUser modeloUsuario;
	
    @Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		imagenModelo = new ModeloImagenSlide(miPool);
		modeloUsuario = new ModeloUser(miPool);
	}

	/**
     * @see HttpServlet#HttpServlet()
     */
    public GetImageInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		int Dec = Integer.parseInt(request.getParameter("Dec"));
		int ID= Integer.parseInt(request.getParameter("ID"));
		
		ImagenSlide imagen = null;
		User u_imagen = null;
		
		InputStream bos=null;
		
		switch(Dec) {
		case 1:
			imagen = imagenModelo.obtenerImagen(ID);
			bos = new ByteArrayInputStream(imagen.getBytesImagen());
			break;
		case 2:
			u_imagen = modeloUsuario.getUserImage(ID);
			bos = new ByteArrayInputStream(u_imagen.getImagenByte());
			break;
		case 3:
			break;
		}
		
		int tamanoInput = bos.available();
        byte[] datosIMAGEN = new byte[tamanoInput];
        bos.read(datosIMAGEN, 0, tamanoInput);

        response.getOutputStream().write(datosIMAGEN);
        bos.close();
       
		/*response.setContentType("image/png");
		OutputStream oImage;
		
		oImage=response.getOutputStream();
		oImage.write(imagen.getBytesImagen());
		oImage.flush();
		oImage.close();*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
