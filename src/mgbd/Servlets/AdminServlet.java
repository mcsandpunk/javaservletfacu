package mgbd.Servlets;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import mgbd.Modales.*;
import mgbd.classes.*;

/**
 * Servlet implementation class AdminServlet
 */
@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="Pool_Connection_BDM")
	
	private DataSource miPool;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	private Cookie[] AllCookies; 
	private ModeloUser modeloUsuario;
	
	
	
    @Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		modeloUsuario = new ModeloUser(miPool);
	}

	public AdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		AllCookies = request.getCookies();
		String ID="0";
		if(AllCookies!=null) {
			for(Cookie temp_Cookie:AllCookies) {
				if("MGBD.Parts.UserADMINID".equals(temp_Cookie.getName())) {
					ID = temp_Cookie.getValue();
					System.out.println("Encontro la cookie");
				}
			}
		}
		
		User usuario;
		
		if(!ID.equals("0")) {
			usuario = modeloUsuario.getUser(Integer.parseInt(ID));
			System.out.println("Se agrega Usuario");
			request.setAttribute("UserInfo",usuario );
			RequestDispatcher miDispatcher = request.getRequestDispatcher("admin.jsp");
			
			miDispatcher.forward(request, response);
		}
		else {
			System.out.println("A Index de nuevo");
			response.sendRedirect(request.getContextPath()+"/IndexServlet");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		
	}

}
