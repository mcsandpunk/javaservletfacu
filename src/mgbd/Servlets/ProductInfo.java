package mgbd.Servlets;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import mgbd.classes.*;
import mgbd.Modales.*;

/**
 * Servlet implementation class ProductInfo
 */
@WebServlet("/ProductInfo")
public class ProductInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="Pool_Connection_BDM")
	
	private DataSource miPool;
       
	ModeloUser modeloUsuario;
	ModeloProduct modeloProducto;
	ModeloImageProduct modeloImgProducto;
	ModeloVideo modeloVideo;
	ModeloCarrito modeloCarrito;
	Cookie[] AllCookies;
	
	
	
    @Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		
		modeloUsuario = new ModeloUser(miPool);
		modeloProducto = new ModeloProduct(miPool);
		modeloImgProducto = new ModeloImageProduct(miPool);
		modeloVideo = new ModeloVideo(miPool);
		modeloCarrito = new ModeloCarrito(miPool);
	}

	/**
     * @see HttpServlet#HttpServlet()
     */
    public ProductInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//System.out.println("ENTRA AL GET");
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		User usuario=null;
		
		AllCookies = request.getCookies();
		String ID="0";
		if(AllCookies!=null) {
			for(Cookie temp_Cookie:AllCookies) {
				if("MGBD.Parts.UserID".equals(temp_Cookie.getName())) {
					ID = temp_Cookie.getValue();
					System.out.println("Encontro la cookie");
				}
			}
		}
	
		if(!ID.equals("0")) {
			usuario = modeloUsuario.getUser(Integer.parseInt(ID));
			System.out.println("Se agrega Usuario en NavBAR");
			request.setAttribute("UserInfo",usuario );
			
			int todosProductos = modeloCarrito.getAllCarrito(usuario.getID());
			request.setAttribute("productos", todosProductos);
			
		}
		
		String logout = request.getParameter("Logout");
	
		if(logout!=null) {
			if(logout.equals("TRUE")) {
				Cookie cookie = new Cookie("MGBD.Parts.UserID","");
				cookie.setMaxAge(0);
				response.addCookie(cookie);
				
				response.sendRedirect(request.getContextPath()+"/IndexServlet");
			}
		}
		
		String ID_Product = request.getParameter("ID");
		System.out.println("ID del Producto: "+ID_Product);
		if(ID_Product==null) {
			ID_Product = "1";
		}
		int idProducto = Integer.parseInt(ID_Product);
		System.out.println("ID DEL PRODUCTO FINAL: "+ID_Product);
		Product producto = modeloProducto.getProduct(Integer.parseInt(ID_Product));
		List<ImageProduct> imagenesProductos = modeloImgProducto.getAllImagesProduct(Integer.parseInt(ID_Product));
		List<Video> videosProductos = modeloVideo.getAllVideoFromProducto(Integer.parseInt(ID_Product));
		request.setAttribute("Producto", producto);
		request.setAttribute("ImgPro",imagenesProductos);
		request.setAttribute("VideosPro", videosProductos);
		
		RequestDispatcher miDispatcher = request.getRequestDispatcher("/ProductInfo.jsp");
		miDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
