package mgbd.Servlets;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import mgbd.Modales.*;
import mgbd.classes.*;


/**
 * Servlet implementation class RequestServlet
 */
@WebServlet("/RequestServlet")
public class RequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private Cookie[] AllCookies;
	@Resource(name="Pool_Connection_BDM")
	
	private DataSource miPool;
	private User usuario;
	private ModeloUser modeloUser;
	private ModeloParts modeloParts;
	private ModeloCarrito modeloCarrito;
	private ModeloSolicitud modeloSolicitud;
	
    @Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		modeloUser = new ModeloUser(miPool);
		modeloParts = new ModeloParts(miPool);
		modeloCarrito = new ModeloCarrito(miPool);
		modeloSolicitud = new ModeloSolicitud(miPool);
	}

	/**
     * @see HttpServlet#HttpServlet()
     */
    public RequestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		AllCookies = request.getCookies();
		String ID="0";
		if(AllCookies!=null) {
			for(Cookie temp_Cookie:AllCookies) {
				if("MGBD.Parts.UserADMINID".equals(temp_Cookie.getName())) {
					ID = temp_Cookie.getValue();
					System.out.println("Encontro la cookie");
				}
				else if("MGBD.Parts.UserID".equals(temp_Cookie.getName())) {
					ID = temp_Cookie.getValue();
					System.out.println("Encontro la cookie");
				}	
			}
		}
		
		if(!ID.equals("0")) {
			usuario = modeloUser.getUser(Integer.parseInt(ID));
			System.out.println("Se agrega Usuario");
			request.setAttribute("UserInfo",usuario );
			
			int todosProductos = modeloCarrito.getAllCarrito(usuario.getID());
			
			System.out.println("Cantidad de articulos del Usuario: "+ todosProductos);
			
			request.setAttribute("productos", todosProductos);
			
			List<Parts> ListaParts;
			try {
				System.out.println("Obtiene Parts");
				ListaParts=modeloParts.getParts();
				request.setAttribute("ListaParts",ListaParts);
				
			}catch(Exception e) {
				
				e.printStackTrace();
			}
			
			if(usuario.getNivel()==1) {
				System.out.println("Nivel 1");
				List<ViewRequest> pendientes = modeloSolicitud.getSolicitud("Revision");
				List<ViewRequest> aprobados = modeloSolicitud.getSolicitud("Aprobado");
				List<ViewRequest> rechazados = modeloSolicitud.getSolicitud("Rechazado");
				System.out.println("Pendientes: "+pendientes.size());
				if(pendientes!=null) {
					for(int i=0;i<pendientes.size();i++) {
						if(pendientes.get(i).getID_User()!=usuario.getID()) {
							pendientes.remove(i);
						}
					}
				}
				
				System.out.println("Pendientes: "+pendientes.size());
				System.out.println("Aprobados: "+aprobados.size());
				if(aprobados!=null) {
					for(int i=0;i<aprobados.size();i++) {
						if(aprobados.get(i).getID_User()!=usuario.getID()) {
							aprobados.remove(i);							
						}						
					}
				}
				
				if(aprobados!=null) {
					for(int i=0;i<aprobados.size();i++) {
						if(aprobados.get(i).getEstado().equals("TRUE")) {
							aprobados.remove(i);							
						}						
					}
				}
				
				System.out.println("Aprobados: "+aprobados.size());
				System.out.println("Rechazados: "+rechazados.size());
				if(rechazados!=null) {
					for(int i=0;i<rechazados.size();i++) {
						if(rechazados.get(i).getID_User()!=usuario.getID()) {
							rechazados.remove(i);
						}
					}
				}
				
				System.out.println("Rechazados: "+rechazados.size());
				
				int cantidadTotal;
				int cantidadPend;
				int cantidadDeclin;
				int cantidadApp;
				if(pendientes!=null) {
					cantidadPend= pendientes.size();
				}else {
					cantidadPend =0;
				}
				request.setAttribute("TotalPendingRequest", cantidadPend);
				
				if(aprobados!=null) {
					cantidadApp= aprobados.size();
				}else {
					cantidadApp =0;
				}
				request.setAttribute("TotalApproRequest", cantidadApp);
				
				if(rechazados!=null) {
					cantidadDeclin= rechazados.size();
				}else {
					cantidadDeclin =0;
				}
				request.setAttribute("TotalDeclinedRequest", cantidadDeclin);
				
				
				request.setAttribute("TotalRequest", cantidadDeclin+cantidadApp+cantidadPend);
				
				request.setAttribute("pendientes", pendientes);
				request.setAttribute("aprobados", aprobados);
				request.setAttribute("rechazados", rechazados);
				
			}			
			else if(usuario.getNivel()==0) {
				
				String ID_Accept = request.getParameter("IDRequestAccept");
				String ID_Delete = request.getParameter("IDRequestDecline");
				
				if(ID_Accept!=null) {
					if(ID_Accept.length()>0) {
						modeloSolicitud.setEstadoSolicitud(Integer.parseInt(ID_Accept), "Aprobado");
					}
				}
				if(ID_Delete!=null) {
					if(ID_Delete.length()>0) {
						modeloSolicitud.setEstadoSolicitud(Integer.parseInt(ID_Delete), "Rechazado");
					}
				}
				
				List<ViewRequest> pendientes = modeloSolicitud.getSolicitud("Revision");
				List<ViewRequest> aprobados = modeloSolicitud.getSolicitud("Aprobado");
				List<ViewRequest> rechazados = modeloSolicitud.getSolicitud("Rechazado");
				
				int cantidadTotal;
				int cantidadPend;
				int cantidadDeclin;
				int cantidadApp;
				if(pendientes!=null) {
					cantidadPend= pendientes.size();
				}else {
					cantidadPend =0;
				}
				request.setAttribute("TotalPendingRequest", cantidadPend);
				
				if(aprobados!=null) {
					cantidadApp= aprobados.size();
				}else {
					cantidadApp =0;
				}
				request.setAttribute("TotalApproRequest", cantidadApp);
				
				if(rechazados!=null) {
					cantidadDeclin= rechazados.size();
				}else {
					cantidadDeclin =0;
				}
				request.setAttribute("TotalDeclinedRequest", cantidadDeclin);
				
				
				request.setAttribute("TotalRequest", cantidadDeclin+cantidadApp+cantidadPend);
				
				request.setAttribute("pendientes", pendientes);
				request.setAttribute("aprobados", aprobados);
				request.setAttribute("rechazados", rechazados);
				
			}
			
			String ID_SolicitudRequest = request.getParameter("ID_S");
			if(ID_SolicitudRequest!=null) {
				if(ID_SolicitudRequest.length()>0) {
					int IDSolicitud = Integer.parseInt(ID_SolicitudRequest);
					List<Carrito> productos = modeloSolicitud.getProductoRequest(IDSolicitud);
					double precioTotal = modeloSolicitud.getTotalSolicitud(IDSolicitud);
					
					request.setAttribute("ID_Solicitud", IDSolicitud);
					request.setAttribute("precioTotal", precioTotal);
					request.setAttribute("ReqProductos", productos);
				}
			}
			
			String ID_SoliDelete = request.getParameter("ID_R");
			if(ID_SoliDelete!=null) {
				if(ID_SoliDelete.length()>0) {
					int IDSolicitud = Integer.parseInt(ID_SoliDelete);
					List<Carrito> productos = modeloSolicitud.getProductoRequest(IDSolicitud);
					double precioTotal = modeloSolicitud.getTotalSolicitud(IDSolicitud);
					
					request.setAttribute("ID_Solicitud", IDSolicitud);
					request.setAttribute("precioTotalDel", precioTotal);
					request.setAttribute("DelProductos", productos);
				}
			}
			
			String ID_Appro = request.getParameter("ID_A");
			if(ID_Appro!=null) {
				if(ID_Appro.length()>0) {
					int IDSolicitud = Integer.parseInt(ID_Appro);
					List<Carrito> productos = modeloSolicitud.getProductoRequest(IDSolicitud);
					double precioTotal = modeloSolicitud.getTotalSolicitud(IDSolicitud);
					
					request.setAttribute("ID_Solicitud", IDSolicitud);
					request.setAttribute("precioTotalApp", precioTotal);
					request.setAttribute("AppProductos", productos);
				}
			}
			
			RequestDispatcher miDispatcher = request.getRequestDispatcher("/Request.jsp");
			miDispatcher.forward(request, response);
		}
		else {
			System.out.println("A Index de nuevo");
			response.sendRedirect(request.getContextPath()+"/IndexServlet");
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		System.out.println("ENTRA A POST");
		if(!checkDatos(request,response)) {
			System.out.println("Error en Checkout");
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("Error");
			return;
		}
		
		String name= request.getParameter("ID_Name");
		String ape = request.getParameter("ID_Ape");
		String email = request.getParameter("ID_Email");
		String calle= request.getParameter("ID_Street");
		String numInt = request.getParameter("ID_IntNumber");
		String CP = request.getParameter("ID_CP");
		
		String pago = request.getParameter("pago");
		
		if(pago.equals("Card")) {
			String CardN = request.getParameter("ID_Card_Name");
			String CardNum = request.getParameter("ID_Card_Number");
			String CardExp = request.getParameter("ID_Card_Exp");
			String CardCVC = request.getParameter("ID_Card_CVC");
		}
		
		String ID= request.getParameter("IDSOLI");
		String Metodo;
		if(pago.equals("Card")) {
			Metodo = "Tarjeta";
		}
		else {
			Metodo = "Efectivo";
		}
		
		System.out.println("Llego a updateSolicitud");
		
		modeloSolicitud.updateSolicitudCompra_MetodoP_Estado("TRUE", Metodo, "Aprobado");
		
		
	}
	
	private boolean checkDatos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String name= request.getParameter("ID_Name");
		String ape = request.getParameter("ID_Ape");
		String email = request.getParameter("ID_Email");
		String calle= request.getParameter("ID_Street");
		String numInt = request.getParameter("ID_IntNumber");
		String CP = request.getParameter("ID_CP");
		String CardN = request.getParameter("ID_Card_Name");
		String CardNum = request.getParameter("ID_Card_Number");
		String CardExp = request.getParameter("ID_Card_Exp");
		String CardCVC = request.getParameter("ID_Card_CVC");
		
		if(!FuncionesVarias.ValidarLongitud(name, 5)) {
			return false;
		}
		
		if(!FuncionesVarias.ValidarLongitud(ape, 5)) {
			return false;
		}
		
		if(!FuncionesVarias.ValidarEmail(email)) {
			return false;
		}
		
		if(!FuncionesVarias.ValidarLongitud(calle, 5)) {
			return false;
		}
		
		if(!FuncionesVarias.ValidarLongitud(numInt, 4)) {
			return false;
		}
		
		if(!FuncionesVarias.ValidarLongitud(CP, 4)) {
			return false;
		}
		
		String Pago = request.getParameter("pago");
		
		if(Pago!=null) {
			if(Pago.equals("Card")) {
				if(!FuncionesVarias.ValidarLongitud(CardN, 4)) {
					return false;
				}
				
				if(!FuncionesVarias.ValidarLongitud(CardNum, 4)) {
					return false;
				}
				
				if(!FuncionesVarias.ValidarLongitud(CardExp, 4)) {
					return false;
				}
				
				if(!FuncionesVarias.ValidarLongitud(CardCVC, 4)) {
					return false;
				}
			}
		}
		
		return true;
	}

}
