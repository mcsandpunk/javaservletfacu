package mgbd.Servlets;

import java.io.IOException;
import java.io.InputStream;

import javax.annotation.Resource;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.sql.DataSource;

import mgbd.classes.*;
import mgbd.Modales.*;

/**
 * Servlet implementation class NavbarServlet
 */
@WebServlet("/NavbarServlet")
@MultipartConfig
public class NavbarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	@Resource(name="Pool_Connection_BDM")
	private DataSource miPool;
	
	private ModeloUser modeloUsuario;
	private ModeloCarrito modelCarrito;
	private ModeloProduct modeloProducto;
	
	
	private Cookie[] AllCookies;
	
	
	
    public NavbarServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		modeloUsuario=new ModeloUser(miPool);
		modelCarrito = new ModeloCarrito(miPool);
		modeloProducto = new ModeloProduct(miPool);
	}


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		User usuario=null;
		
		AllCookies = request.getCookies();
		String ID="0";
		if(AllCookies!=null) {
			for(Cookie temp_Cookie:AllCookies) {
				if("MGBD.Parts.UserID".equals(temp_Cookie.getName())) {
					ID = temp_Cookie.getValue();
					System.out.println("Encontro la cookie");
				}
			}
		}
	
		
		
		
		
		if(!ID.equals("0")) {
			usuario = modeloUsuario.getUser(Integer.parseInt(ID));
			System.out.println("Se agrega Usuario en NavBAR");
			request.setAttribute("UserInfo",usuario );
			
		}
		
		
		
		String logout = request.getParameter("Logout");
		
		if(logout.equals("TRUE")) {
			Cookie cookie = new Cookie("MGBD.Parts.UserID","");
			cookie.setMaxAge(0);
			response.addCookie(cookie);
			
			response.sendRedirect(request.getContextPath()+"/IndexServlet");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		User usuario = null;
		System.out.println("Verifica POST");
		try{
			if(LoginSignUp(request,response))
				return;
			System.out.println("Fin de LoginSignUp");
		}catch(IllegalStateException e) {
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("SizeBig");
			System.out.println("DatosMuyLargos");
		}
		
		AllCookies = request.getCookies();
		String ID="0";
		if(AllCookies!=null) {
			for(Cookie temp_Cookie:AllCookies) {
				if("MGBD.Parts.UserID".equals(temp_Cookie.getName())) {
					ID = temp_Cookie.getValue();
					System.out.println("Encontro la cookie");
				}
			}
		}
		
		if(!ID.equals("0")) {
			usuario = modeloUsuario.getUser(Integer.parseInt(ID));
			System.out.println("Se agrega Usuario");
			
		}
		
		String varPro = request.getParameter("varPro");
		System.out.println(varPro);
		
		if(usuario!=null) {
			if(varPro!=null) {
				int cantidad = modelCarrito.isProductInCart(Integer.parseInt(varPro), usuario.getID());
				System.out.println("Cantidad total Carrito: "+Integer.toString(cantidad));
				if(cantidad<=0) {
					System.out.println("Cantidad menos cero");
					System.out.println("ID Producto: "+varPro);
					
					//System.out.println("ID Usuario: "+Integer.toString(usuario.getID()));

					System.out.println("Imprimo");
					System.out.println("Imprimo");
					System.out.println("Imprimo");
					System.out.println("Imprimo");
					System.out.println("Imprimo");
					System.out.println("Imprimo");
					
					//float bla = modeloProducto.getPriceProduct(1);
					//System.out.println("Precio del producto: "+ bla);
					modelCarrito.addCarritoPAPW(1, 
							Integer.parseInt(varPro), 
							usuario.getID(),
							"Espera");
				}
				else {
					System.out.println("Cantidad mas cero");
					modelCarrito.updateCantidadCarrito(Integer.parseInt(varPro), usuario.getID(), modelCarrito.getCantidadProducto(Integer.parseInt(varPro),usuario.getID())+1);
				}
				
				int todosProductos = modelCarrito.getAllCarrito(usuario.getID());
				
				System.out.println("EL ID del Usuario: "+Integer.toString(usuario.getID())+"\n Tiene "+todosProductos+" productos en su carrito");
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write(Integer.toString(todosProductos));
				
				return;
			}
		}else {
			
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("NoUserAdd");
			
			return;
			
		}
		
		

	}
	
	private boolean LoginSignUp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String Form_Selected = request.getParameter("Form");
		System.out.println(Form_Selected);
		System.out.println("Entra a la funcion general");
		if(Form_Selected!=null) {
			if(Form_Selected.equals("SignUp")) {
				System.out.println("Entra al apartado de SignUp");
				try{
					SignUp(request,response);
					return true;
				}catch(NullPointerException e) {
					response.setContentType("text/plain");
					response.setCharacterEncoding("UTF-8");
					response.getWriter().write("SizeBig");
					return false;
				}
			}
			else if(Form_Selected.equals("Login")) {
				System.out.println("Entra al apartado de Login");
				Login(request,response);
				
				return true;
			}
		}
		
		return false;
	}
	
	private void Login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String email = request.getParameter("email");
		String contra = request.getParameter("psw");
		
		User usuario_Obtenido=modeloUsuario.CheckUser(email, contra);
		
		if(usuario_Obtenido!=null) {
			
			if(usuario_Obtenido.getNivel()==0) {
				Cookie userCookie = new Cookie("MGBD.Parts.UserADMINID",Integer.toString(usuario_Obtenido.getID()));				
				userCookie.setMaxAge(60*60);
				response.addCookie(userCookie);
				
				//response.sendRedirect(request.getContextPath()+"/AdminServlet");
				
				System.out.println("Admin logeado");
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write("AdminFound");
				return;
			}
			else {
				Cookie userCookie = new Cookie("MGBD.Parts.UserID",Integer.toString(usuario_Obtenido.getID()));
				userCookie.setMaxAge(60*60);
				response.addCookie(userCookie);
				
				//response.sendRedirect(request.getContextPath()+"/IndexServlet");
				
				System.out.println("Usuario logeado");
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write("UserFound");
				return;
			}
			
		}
		else {
			System.out.println("Usuario no existe");
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("UserNotFound");
			return;
		}
		
	}
	
	
	private void SignUp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("Entras a la funcion SignUp");
		User usuario = new User();
		
		String email = request.getParameter("email");		
		
		String nick = request.getParameter("nick");
		if(!FuncionesVarias.ValidarLongitud(nick, 4)) {
			System.out.println("No hay nickname");
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("noNick");
			return;
		}
		
		String contra = request.getParameter("psw");
		if(!FuncionesVarias.ValidarLongitud(contra, 4)) {
			System.out.println("No hay Contrasena");
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("noContra");
			
			return;
		}
		
		String contrarep = request.getParameter("repsw");
	
		
		if(contra.equals(contrarep)){
			Part part = null;
			try{
				part = request.getPart("foto");
				
			}catch(IllegalStateException e) {
				System.out.println("Demasiado Grandes los archivos");
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write("SizeBig");
				
				return;
			}
			
			InputStream inputStream = null;
			if(part.getSize() >0) {
				inputStream = part.getInputStream();
				if(inputStream != null) {
					usuario.setImagen(inputStream);
				}
			}
			
			if(FuncionesVarias.ValidarEmail(email.trim())) {
				
				usuario.setEmail(email.trim());
				usuario.setNickname(nick.trim());
				usuario.setEstado(true);
				usuario.setNivel((short)1);
				
				String resp = modeloUsuario.AddUser(usuario, contra);
				
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write(resp);
				//response.sendRedirect(request.getContextPath()+"/IndexServlet");
				return;
			}
			else {
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write("NoEmail");
				System.out.println("Email no valido");
				return;
			}
		}
		else {
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("PassNoEqual");
			System.out.println("Contras no coincide");
			return;
		}
		
		
	}

}
