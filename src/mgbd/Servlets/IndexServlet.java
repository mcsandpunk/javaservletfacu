package mgbd.Servlets;


import java.io.IOException;

import java.util.List;

import javax.annotation.Resource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;

import mgbd.classes.*;
import mgbd.Modales.*;

/**
 * Servlet implementation class IndexServlet
 */
@WebServlet("/IndexServlet")
@MultipartConfig
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private ModeloParts modeloParts;
	private ModeloImagenSlide modeloSlide;
	private ModeloUser modeloUsuario;
	private ModeloProduct modeloProduct;
	private ModeloImageProduct modeloImagenProducto;
	private ModeloCarrito modelCarrito;
	//Establecer el DataSource
	@Resource(name="Pool_Connection_BDM")
	
	private DataSource miPool;
	
	Cookie[] AllCookies;
	
	public void PoolConect()throws IllegalArgumentException, NamingException{
		
		InitialContext initialContext = new InitialContext();
		Context context = (Context) initialContext.lookup("java:comp/env");
		miPool = (DataSource) context.lookup("Pool_Connection_BDM");
		
	}
	
    @Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		
		try {
			//PoolConect();
			modeloParts=new ModeloParts(miPool);
			modeloSlide=new ModeloImagenSlide(miPool);
			modeloUsuario = new ModeloUser(miPool);
			modeloProduct = new ModeloProduct(miPool);
			modeloImagenProducto = new ModeloImageProduct(miPool);
			modelCarrito = new ModeloCarrito(miPool);
		}catch (Exception e) {
			throw new ServletException(e);
		}
	}

	/**
     * @see HttpServlet#HttpServlet()
     */
    public IndexServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		
		AllCookies = request.getCookies();
		String ID="0";
		if(AllCookies!=null) {
			for(Cookie temp_Cookie:AllCookies) {
				if("MGBD.Parts.UserID".equals(temp_Cookie.getName())) {
					ID = temp_Cookie.getValue();
					System.out.println("Encontro la cookie");
				}
			}
		}
	
		User usuario;
		
		if(!ID.equals("0")) {
			usuario = modeloUsuario.getUser(Integer.parseInt(ID));
			System.out.println("Usuario Inicia Sesion");
			request.setAttribute("UserInfo",usuario );
			
			int todosProductos = modelCarrito.getAllCarrito(usuario.getID());
			
			System.out.println("Cantidad de articulos del Usuario: "+ todosProductos);
			
			request.setAttribute("productos", todosProductos);
		}		
		
		
		List<Parts> ListaParts;
		try {
			System.out.println("Obtiene Parts");
			ListaParts=modeloParts.getParts();
			request.setAttribute("ListaParts",ListaParts);
			
		}catch(Exception e) {
			
			e.printStackTrace();
		}
		
		List<Product> ListaProductos;
		try {
			
			System.out.println("Obtiene Productos");
			ListaProductos = modeloProduct.getProducts(5);
			for(Product producto:ListaProductos) {
				ImageProduct imagen = new ImageProduct();
				
				System.out.println("Obtiene Loca de Imagen Producto");
				imagen.setLocalizacion(modeloImagenProducto.getProductImage(producto.getID()));
				
				producto.setDireccionImagen(imagen.getLocalizacion());
			}
			request.setAttribute("ListaProductos", ListaProductos);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		List<ImagenSlide> imagenesSlide;
		try {
			System.out.println("Obtiene SlideImagenes");
			imagenesSlide = modeloSlide.obtenerImagenes();
			
			request.setAttribute("ImagenesSlide", imagenesSlide);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		RequestDispatcher miDispatcher = request.getRequestDispatcher("/index.jsp");		
		miDispatcher.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		
	}
}


