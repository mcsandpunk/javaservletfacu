package mgbd.Servlets;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import mgbd.Modales.*;
import mgbd.classes.*;

/**
 * Servlet implementation class CartServlet
 */
@WebServlet("/CartServlet")
public class CartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Resource(name="Pool_Connection_BDM")	
	private DataSource miPool;
	
	private Cookie[] AllCookies; 
	ModeloUser modeloUsuario;
	ModeloCarrito modeloCarrito;
	ModeloSolicitud modeloSolicitud;
	User usuario;
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	
    public CartServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		modeloUsuario = new ModeloUser(miPool);
		modeloCarrito = new ModeloCarrito(miPool);
		modeloSolicitud = new ModeloSolicitud(miPool);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		
		System.out.println("Entro al get de CartServlet");
		AllCookies = request.getCookies();
		String ID="0";
		if(AllCookies!=null) {
			for(Cookie temp_Cookie:AllCookies) {
				if("MGBD.Parts.UserID".equals(temp_Cookie.getName())) {
					ID = temp_Cookie.getValue();
					System.out.println("Encontro la cookie");
				}
			}
		}
		
		
		
		if(!ID.equals("0")) {
			usuario = modeloUsuario.getUser(Integer.parseInt(ID));
			System.out.println("Se agrega Usuario");
			request.setAttribute("UserInfo",usuario );
			
			String ID_eliminar=request.getParameter("DeleteID");
			if(ID_eliminar!=null) {
				if(FuncionesVarias.ValidarNumeros(ID_eliminar)) {
					modeloCarrito.eliminarCarrito(Integer.parseInt(ID_eliminar));
				}
			}
			
			List<CarritoView> carritos= modeloCarrito.getCarritoView(usuario.getID());
			request.setAttribute("carritos", carritos);
			
			int todosProductos = modeloCarrito.getAllCarrito(usuario.getID());
			request.setAttribute("productos", todosProductos);
			
			
			RequestDispatcher miDispatcher = request.getRequestDispatcher("/Cart.jsp");
			miDispatcher.forward(request, response);
			return;
		}else {
			
			System.out.println("A Index de nuevo");
			response.sendRedirect(request.getContextPath()+"/IndexServlet");
			return;
		}
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		System.out.println("DOPOST");
		System.out.println("DOPOST");
		System.out.println("DOPOST");
		System.out.println("DOPOST");
		
		String nombre = request.getParameter("Send");
		System.out.println("Enviado: "+nombre);
		if(nombre.equals("Send Request")) {
			ValidarEnviado(request,response);
			response.sendRedirect(request.getContextPath()+"/CartServlet");
		}
		else if(nombre.equals("Erase All")) {
			
			String[] ID_Carrito = request.getParameterValues("Carrito");
			for(int i =0;i<ID_Carrito.length;i++) {
				modeloCarrito.eliminarCarrito(Integer.parseInt(ID_Carrito[i]));
			}
			response.sendRedirect(request.getContextPath()+"/CartServlet");
		}else if(nombre.equals("Update Cart")) {
			
			actualizarCarrito(request, response);
			response.sendRedirect(request.getContextPath()+"/CartServlet");
		}
		
		
	}
	
	private void ValidarEnviado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		if(usuario!=null) {
			actualizarCarrito(request, response);
			modeloSolicitud.crearSolicitud();
			int ID_Solicitud = modeloSolicitud.getLastIDSolicitud();
			
			modeloCarrito.enviarCarrito(usuario.getID(), ID_Solicitud);
		}
	}
	
	private void actualizarCarrito(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Inicia Actualizar");
		String[] cantidad = request.getParameterValues("qnt");
		String[] ID_Carrito = request.getParameterValues("Carrito");
		String[] sugPrice = request.getParameterValues("sugprice");
		String[] ID_Producto = request.getParameterValues("IDPro");
		
		for(int i =0;i<cantidad.length;i++) {
			int cant = Integer.parseInt(cantidad[i]);
			int IdPro = Integer.parseInt(ID_Producto[i]);
			
			double pre = Double.parseDouble(sugPrice[i]);
		
			int IDC = Integer.parseInt(ID_Carrito[i]);
			modeloCarrito.updateCarrito(cant, IdPro,usuario.getID(), "Espera",
					pre, IDC);
			
		}
		
	}

}
