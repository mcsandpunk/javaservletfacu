package mgbd.Modales;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.sql.*;

import mgbd.classes.Product;

public class ModeloProduct {

	private DataSource BD;
	
	public ModeloProduct(DataSource origen) {
		this.BD=origen;
	}
	
	public double getPriceProduct(int ID) {
		
		System.out.println("Inicia funcion");
		Connection cn=null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		
		try {
			
			cn = BD.getConnection();
			ps = cn.prepareStatement("select getPrecioProduct(?)");
			
			ps.setInt(1, ID);
			
			rs=ps.executeQuery();
			
			if(rs.next()) {
				return rs.getDouble(1);
			}
			cn.close();
			ps.close();
			cn=null;
			System.out.println("Final funcion");
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		return 0;
	}
	
	public void UpdateProduct(int ID,String Articulo,String Descripcion,int Unidad,boolean estado,
			int FK_P,int FK_Cat,boolean publicado,double precio) {
		Connection cn=null;
		CallableStatement cs=null;
		
		try {
			cn=BD.getConnection();
			cs=cn.prepareCall("{call UpProduct(?,?,?,?,?,?,?,?,?)}");
			
			cs.setInt(1, ID);
			cs.setString(2,Articulo);
			cs.setString(3, Descripcion);
			cs.setInt(4, Unidad);
			cs.setBoolean(5, estado);
			cs.setInt(6, FK_P);
			cs.setInt(7, FK_Cat);
			cs.setBoolean(8, publicado);
			cs.setDouble(9,precio);
			
			cs.execute();
			
			cn.close();
			cs.close();
			cn=null;
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
	}
	
	public List<Product> getAllAdminProduct() {
		List<Product> productos= new ArrayList<>();
		Connection cn=null;
		CallableStatement cs=null;
		ResultSet rs=null;
		
		try {
			
			cn = BD.getConnection();
			
			cs = cn.prepareCall("{call getViewProductos()}");
			
			rs = cs.executeQuery();
			
			while(rs.next()) {
				Product Producto = new Product();
				
				Producto.setID(rs.getInt("ID_Producto"));
				Producto.setArticulo(rs.getString("Articulo"));
				Producto.setsCategoria(rs.getString("Cate"));
				Producto.setsPart(rs.getString("Part"));
				Producto.setRating(rs.getInt("Valoracion"));
				Producto.setUnidad(rs.getInt("Unidad"));
				
				productos.add(Producto);
			}
			
			cs.close();
			cn.close();
			rs.close();
			
			cn=null;
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		return productos;
	}
	
	public 	void setPublicado(boolean estado,int ID) {
		Connection cn=null;
		PreparedStatement ps =null;
		
		try {
			cn = BD.getConnection();
			ps = cn.prepareStatement("select setPublicado(?,?)");
			
			ps.setBoolean(1, estado);
			ps.setInt(2,ID);
			
			ps.execute();
			
			cn.close();
			ps.close();
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
	}
	
	public List<Product> getProducts(){
		List<Product> productos = new ArrayList<>();
		Connection cn=null;
		CallableStatement cs=null;
		ResultSet rs=null;
		
		try {
			cn=BD.getConnection();
			
			cs = cn.prepareCall("{call getAllProduct()}");
			
			rs=cs.executeQuery();
			
			while(rs.next()) {
				Product Producto= new Product();
				
				Producto.setArticulo(rs.getString("Articulo"));
				Producto.setDescripcion(rs.getString("Descripcion"));
				Producto.setUnidad(rs.getInt("Unidad"));
				Producto.setEstado(rs.getBoolean("Estado"));
				Producto.setVistas(rs.getInt("Vistas"));
				Producto.setFecha_Creacion(rs.getDate("Fecha_Creacion"));
				
				Producto.setRating(getValoracionProducto(Producto.getID()));
				Producto.setiCategoria(getCateProductoID(Producto.getID()));
				Producto.setiPart(getPartProductoID(Producto.getID()));
				
				Producto.setPrecio(rs.getDouble("Precio"));
				
				productos.add(Producto);
				
			}
			
			cn.close();
			cs.close();
			rs.close();
			cn=null;
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		
		return productos;
	}
	
	public List<String> getImagesFromProduct(int ID){
		List<String> imagenes = new ArrayList<>();
		
		Connection cn = null;
		CallableStatement cs= null;
		ResultSet rs = null;
		
		try {
			cn = BD.getConnection();
			cs = cn.prepareCall("{call getAllImageFromProduct(?)}");
			
			cs.setInt(1,ID);
			
			rs = cs.executeQuery();
			
			while(rs.next()) {
				String imagen = new String();
				imagen = rs.getString(1);
				
				imagenes.add(imagen);
			}
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		return imagenes;
	}
	
	public Product getProduct(int ID) {
		Product Producto = null;
		Connection cn=null;
		CallableStatement cs=null;
		ResultSet rs=null;
		
		try {
			cn=BD.getConnection();
			
			cs = cn.prepareCall("{call getProduct(?)}");
			
			cs.setInt(1, ID);
			
			rs=cs.executeQuery();
			
			if(rs.next()) {
				Producto= new Product();
				System.out.println("Get Producto");
				Producto.setID(rs.getInt("ID_Producto"));
				Producto.setArticulo(rs.getString("Articulo"));
				Producto.setDescripcion(rs.getString("Descripcion"));
				Producto.setUnidad(rs.getInt("Unidad"));
				Producto.setEstado(rs.getBoolean("Estado"));
				Producto.setVistas(rs.getInt("Vistas"));
				Producto.setFecha_Creacion(rs.getDate("Fecha_Creacion"));
				Producto.setPublicado(rs.getBoolean("Publicado"));
				
				Producto.setRating(getValoracionProducto(Producto.getID()));
				
				Producto.setiCategoria(getCateProductoID(Producto.getID()));
				Producto.setiPart(getPartProductoID(Producto.getID()));
				Producto.setsCategoria(getCateString(Producto.getiCategoria()));
				
				Producto.setsPart(getPartString(Producto.getiPart()));
				
				Producto.setPrecio(rs.getDouble("Precio"));
			}
			
			cn.close();
			cs.close();
			rs.close();
			cn=null;
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		return Producto;
		
	}
	
	public List<Product> getProducts(int Cantidad) {
		List<Product> productos = new ArrayList<>();
		Connection cn=null;
		CallableStatement cs =null;
		ResultSet rs = null;
		
		try {
			
			cn=BD.getConnection();
			cs = cn.prepareCall("{call getProducts(?)}");
			
			cs.setInt(1, Cantidad);
			
			rs = cs.executeQuery();
			
			while(rs.next()) {
				Product producto = new Product();
				
				producto.setID(rs.getInt("ID_Producto"));
				producto.setArticulo(rs.getString("Articulo"));
				producto.setDescripcion(rs.getString("Descripcion"));
				producto.setUnidad(rs.getInt("Unidad"));
				producto.setFecha_Creacion(rs.getDate("Fecha_Creacion"));
				
				producto.setRating(getValoracionProducto(producto.getID()));
				
				producto.setiCategoria(getCateProductoID(producto.getID()));
				producto.setiPart(getPartProductoID(producto.getID()));
				
				producto.setPrecio(rs.getDouble("Precio"));
				productos.add(producto);
			}
			
			cn.close();
			cs.close();
			rs.close();
			cn=null;
			
			return productos;
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		return productos;
	}
	
	public int getLastID() {
		Connection cn=null;
		PreparedStatement cs =null;
		ResultSet rs =null;
		
		try {
			
			cn = BD.getConnection();
			cs = cn.prepareStatement("select getLastProductRecord()");
			
			rs=cs.executeQuery();
			
			if(rs.next()) {
				return rs.getInt(1);
			}
			
			cn.close();
			cs.close();
			rs.close();
			cn=null;
			
			return 0;
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		return 0;
	}
	
	public void AddProduct(String Articulo,String Descripcion,int Unidad,
			boolean estado,int FK_Part,int Vistas,int FK_Catego,double precio) {
		Connection cn=null;
		CallableStatement cs=null;
		
		
		try {
			
			cn=BD.getConnection();
			
			cs = cn.prepareCall("{call AddProduct(?,?,?,?,?,?,?,?)}");
			
			cs.setString(1, Articulo);
			cs.setString(2, Descripcion);
			cs.setInt(3, Unidad);
			cs.setBoolean(4, estado);
			cs.setInt(5, FK_Part);
			cs.setInt(6, Vistas);
			cs.setInt(7, FK_Catego);
			cs.setDouble(8, precio);
			
			cs.execute();
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
	}
	
	private int getValoracionProducto(int ID) {
		
		Connection cn=null;
		PreparedStatement ps=null;
		
		ResultSet rs= null;
		
		int valoracion = 0;
		try {
			cn=BD.getConnection();
			
			ps = cn.prepareStatement("select getValoracionProducto(?)");
			
			ps.setInt(1, ID);
			
			rs= ps.executeQuery();
			
			if(rs.next()) {
				return rs.getInt(1);
			}
			
			cn.close();
			ps.close();
			rs.close();
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		return valoracion;
	}
	
	private String getPartString(int ID) {
		String nombre = null;
		Connection cn=null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			cn = BD.getConnection();
			ps = cn.prepareStatement("select getPartName(?)");
			ps.setInt(1,ID);
			
			rs=ps.executeQuery();
			
			if(rs.next()) {
				System.out.println("Lo que obtengo: "+rs.getString(1));
				nombre = rs.getString(1);
			}
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		return nombre;
	}
	
	private String getCateString(int ID) {
		String nombre = null;
		Connection cn=null;
		PreparedStatement ps=null;
		ResultSet rs = null;
		
		try {
			
			cn = BD.getConnection();
			ps = cn.prepareStatement("select getCategoryName(?)");
			ps.setInt(1, ID);
			
			rs = ps.executeQuery();
			
			if(rs.next()) {
				nombre = rs.getString(1);
			}
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		return nombre;
	}
	
	private int getCateProductoID(int ID) {
		int resultado = 0;
		
		Connection cn=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
				
		try{
			
			cn=BD.getConnection();
			ps = cn.prepareStatement("select getCateProductoID(?)");
			
			ps.setInt(1, ID);
			
			rs = ps.executeQuery();
			
			if(rs.next()) {
				return rs.getInt(1);
			}
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		return resultado;
		
	}
	
	private int getPartProductoID(int ID) {
		int resultado = 0;
		
		Connection cn=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		
		try{
			
			cn=BD.getConnection();
			ps = cn.prepareStatement("select getPartProductoID(?)");
			
			ps.setInt(1, ID);
			
			rs = ps.executeQuery();
			
			if(rs.next()) {
				return rs.getInt(1);
			}
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		return resultado;
	}
	
}
