package mgbd.Modales;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import mgbd.classes.ImagenSlide;


public class ModeloImagenSlide {

	private DataSource BD;
	
	public ModeloImagenSlide(DataSource origen) {
		this.BD = origen;
	}
	
	public void insertImageSlide(ImagenSlide imagen) {
		Connection cn=null;
		PreparedStatement ps=null;
		
		try {
			cn=BD.getConnection();
			ps = cn.prepareStatement("select addImagenSlide(?)");
			System.out.println("ENTRA A ADDIMAGENSLIDE");
			ps.setBlob(1, imagen.getImagen());
			
			ps.execute();
			
			cn.close();
			ps.close();
			cn=null;
		}catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
            	ps.close();
            	
               cn.close();
               cn = null;
            } catch (Exception ex) {
            }
        }
	}
	
	public int getAllImagesSlide() {
		int cantidad=0;
		Connection cn=null;
		PreparedStatement ps=null;
		ResultSet rs = null;
		try {
			
			cn=BD.getConnection();
			ps = cn.prepareStatement("select countAllImages()");
			
			rs = ps.executeQuery();
			
			if(rs.next()) {
				cantidad = rs.getInt(1);
			}
			
			cn.close();
			ps.close();
			
			cn=null;
			
			
		}catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
            	ps.close();
            	
               cn.close();
               cn = null;
            } catch (Exception ex) {
            }
        }
		
		return cantidad;
	}
	
	public void deleteImagenSlide(int ID) {
		Connection cn=null;
		PreparedStatement ps =null;
		try {
			
			cn = BD.getConnection();
			ps = cn.prepareStatement("select deleteImagenSlide(?)");
			ps.setInt(1, ID);
			
			ps.execute();
			
			cn.close();
			ps.close();
			cn=null;
			
		}catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
            	ps.close();
            	
               cn.close();
               cn = null;
            } catch (Exception ex) {
            }
        }
	}
	
	public ImagenSlide obtenerImagen(int ID) {
		ImagenSlide imagen= new ImagenSlide();
		PreparedStatement ps = null;
		ResultSet rs = null;
		OutputStream os= null;
		Connection miConexion=null;
		try {
			miConexion = BD.getConnection();
			String sql = "Select * from imagenslide where ID_ImagenSlide=?;";
			ps=miConexion.prepareStatement(sql);
			ps.setInt(1, ID);
			rs=ps.executeQuery();
			if(rs.next()) {
				imagen.setID(rs.getInt(1));
				imagen.setBytesImagen(rs.getBytes(2));
			}
			miConexion.close();
			miConexion = null;
		}catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
            	ps.close();
            	rs.close();
               miConexion.close();
               miConexion = null;
            } catch (Exception ex) {
            }
        }
		return imagen;
	}
	
	public List<ImagenSlide> obtenerImagenes(){
		List<ImagenSlide> imagenes = new ArrayList<ImagenSlide>();
		PreparedStatement  miStatement=null;
		ResultSet miResult = null;
		Connection miConexion=null;
		try{
			miConexion=BD.getConnection();
			String sql = "SELECT * FROM imagenslide;";
			
			miStatement = miConexion.prepareStatement(sql);
			
			miResult = miStatement.executeQuery();
			while(miResult.next()) {
				ImagenSlide imagen = new ImagenSlide();
				imagen.setID(miResult.getInt(1));
				imagen.setBytesImagen(miResult.getBytes(2));			
				
				imagenes.add(imagen);
			}			
			miConexion.close();
			miConexion=null;
		}catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
            	miStatement.close();
            	miResult.close();
                miConexion.close();
                miConexion=null;
            } catch (Exception ex) {
            }
        }
		return imagenes;
	}
}
