package mgbd.Modales;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import mgbd.classes.CarritoView;

public class ModeloCarrito {

	DataSource BD;	
	
	public ModeloCarrito(DataSource origen) {
		this.BD = origen;
	}
	
	public void eliminarCarrito(int ID) {
		Connection cn=null;
		PreparedStatement ps=null;
		
		try {
			
			cn = BD.getConnection();
			
			ps = cn.prepareStatement("select eliminarCarrito(?)");
			ps.setInt(1, ID);
			
			ps.execute();
			
			cn.close();
			ps.close();
			cn =null;
						
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
	}
	
	public void updateCarrito(int cantidad,int producto,int usuario,
			String Estado,double precio,int carrito) {
		Connection cn=null;
		PreparedStatement ps=null;
		
		try {
			cn=BD.getConnection();
			ps = cn.prepareStatement("select updateCarrito(?,?,?,?,?,?)");
			ps.setInt(1, cantidad);
			ps.setInt(2, producto);
			ps.setInt(3, usuario);
			ps.setString(4, Estado);
			ps.setDouble(5, precio);
			ps.setInt(6, carrito);
			
			ps.execute();
			
			cn.close();
			ps.close();
			cn=null;
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
	}
	
	public void enviarCarrito(int ID_U,int ID_S) {
		Connection cn=null;
		PreparedStatement ps = null;
		try {
			
			cn = BD.getConnection();
			
			ps = cn.prepareStatement("select enviarCarrito(?,?)");
			ps.setInt(1,ID_U);
			ps.setInt(2, ID_S);
			
			ps.execute();
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
	}
	
	public List<CarritoView> getCarritoView(int ID){
		List<CarritoView> carritos = new ArrayList<>();
		Connection cn=null;
		CallableStatement cs=null;
		ResultSet rs=null;
		try {
			
			cn= BD.getConnection();
			
			cs = cn.prepareCall("{call getUserCarrito(?)}");
			cs.setInt(1, ID);
			rs = cs.executeQuery();
			
			while(rs.next()) {
				CarritoView carrito = new CarritoView();
				
				carrito.setArticulo(rs.getString("ArtP"));
				carrito.setCantidad(rs.getInt("Cant"));
				carrito.setID_Carrito(rs.getInt("IDC"));
				carrito.setID_Producto(rs.getInt("IDP"));
				carrito.setImagen(rs.getString("Imagen"));
				carrito.setPrecio(rs.getInt("Precio"));
				
				carritos.add(carrito);
			}
			
			cn.close();
			cs.close();
			rs.close();
			cn=null;
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		return carritos;
	}
	
	public int getCantidadProducto(int ID_P,int ID_U) {
		int cantidad=0;
		Connection cn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			cn = BD.getConnection();
			ps = cn.prepareStatement("select getCantidadProductCarrito(?,?)");
			
			ps.setInt(1, ID_P);
			ps.setInt(2,ID_U);
			
			rs = ps.executeQuery();
			
			if(rs.next()) {
				cantidad = rs.getInt(1);
			}
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		return cantidad;
	}
	
	public int getAllCarrito(int ID_U){
		int cantidad=0;
		Connection cn=null;
		PreparedStatement ps=null;
		ResultSet rs =null;
		try {
		
			cn = BD.getConnection();
			ps = cn.prepareStatement("select getAllCarrito(?)");
			
			ps.setInt(1, ID_U);
			
			rs = ps.executeQuery();
			
			if(rs.next()) {
				cantidad = rs.getInt(1);
			}
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		return cantidad;
	}
	
	public int updateCantidadCarrito(int ID_P,int ID_U,int unidades) {
		int cantidad =0;
		Connection cn=null;
		PreparedStatement ps=null;
		
		try {
			cn = BD.getConnection();
			ps = cn.prepareStatement("select updateCantidadCarrito(?,?,?)");
			
			ps.setInt(1, ID_P);
			ps.setInt(2, ID_U);
			ps.setInt(3, unidades);
			
			ps.execute();
			
			cn.close();
			ps.close();
			
			cn=null;
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		return cantidad;
	}
	
	public int isProductInCart(int ID_P,int ID_U) {
		int cantidad =0;
		Connection cn = null;
		PreparedStatement ps= null;
		ResultSet rs=null;
		
		try {
			
			cn = BD.getConnection();
			ps = cn.prepareStatement("select isProductInCart(?,?)");
			
			ps.setInt(1, ID_P);
			ps.setInt(2, ID_U);
			
			rs = ps.executeQuery();
			
			if(rs.next()) {
				System.out.println("ENTRO A NEXT()");
				cantidad = rs.getInt(1);
			}
			
			System.out.println("Cantidad en Carrito: " +cantidad);
			
			ps.close();
			cn.close();
			rs.close();
			cn=null;
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		return cantidad;
	}
	
	public void addCarrito(int Cantidad,int FK_P,int FK_U,String Estado,double precio) 
	{
		Connection cn=null;
		PreparedStatement ps = null;
		System.out.println("Entra AddCarrito");
		try {
			System.out.println("Cantidad:" + Cantidad);
			System.out.println("FK: "+FK_P);
			System.out.println("FKU: " +FK_U);
			System.out.println("Estado:"+Estado);
			System.out.println("Precio:"+precio);
			
			cn=BD.getConnection();
			
			ps = cn.prepareStatement("select addCarrito(?,?,?,?,?)");
			
			ps.setInt(1, Cantidad);
			ps.setInt(2, FK_P);
			ps.setInt(3, FK_U);
			ps.setString(4,Estado);
			ps.setDouble(5, precio);
			
			ps.execute();
			
			cn.close();
			ps.close();
			
			cn=null;
			
			System.out.println("Fin de AddCarrito");
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println("ErrorenAdd");
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
	}
	
	public void addCarritoPAPW(int Cantidad,int FK_P,int FK_U,String Estado) 
	{
		Connection cn=null;
		PreparedStatement ps = null;
		System.out.println("Entra AddCarrito PAPW");
		try {
			System.out.println("Cantidad:" + Cantidad);
			System.out.println("FK: "+FK_P);
			System.out.println("FKU: " +FK_U);
			System.out.println("Estado:"+Estado);
			
			cn=BD.getConnection();
			
			ps = cn.prepareStatement("select addCarritoPAPW(?,?,?,?)");
			
			ps.setInt(1, Cantidad);
			ps.setInt(2, FK_P);
			ps.setInt(3, FK_U);
			ps.setString(4,Estado);
			
			ps.execute();
			
			cn.close();
			ps.close();
			
			cn=null;
			
			System.out.println("Fin de AddCarrito PAPW");
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println("ErrorenAdd");
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
	}
}
