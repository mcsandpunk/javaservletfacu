package mgbd.Modales;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.sql.*;

import mgbd.classes.Carrito;
import mgbd.classes.CarritoView;
import mgbd.classes.ViewRequest;

public class ModeloSolicitud {

	DataSource BD;
	
	public ModeloSolicitud(DataSource origen) {
		this.BD = origen;
	}
	
	public void updateSolicitudCompra_MetodoP_Estado(String Compra,String Metodo,String Estado) {
		
		Connection cn=null;
		PreparedStatement ps = null;
		
		try {
			
			cn = BD.getConnection();
			ps = cn.prepareStatement("select updateSolicitudCompra_MetodoP_Estado(?,?,?)");
			ps.setString(1, Estado);
			ps.setString(2, Metodo);
			ps.setString(3, Compra);
			
			ps.execute();
			
			cn.close();
			ps.close();
			cn=null;
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
	}
	
	public int getCantidadProductosSolicitud(int ID) {
		int cantidad=0;
		Connection cn=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		
		try {
			cn = BD.getConnection();
			ps = cn.prepareStatement("select getCantidadProductosSolicitud(?)");
			ps.setInt(1, ID);
			
			rs = ps.executeQuery();
			
			if(rs.next()) {
				cantidad = rs.getInt(1);
			}
			cn.close();
			rs.close();
			ps.close();
			cn=null;
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		return cantidad;
	}
	
	public void setEstadoSolicitud(int ID,String Estado) {
		Connection cn=null;
		PreparedStatement ps=null;
		
		try {
			
			cn=BD.getConnection();
			ps = cn.prepareStatement("select setEstadoSolicitud(?,?)");
			ps.setInt(1, ID);
			ps.setString(2, Estado);
			ps.execute();
			
			cn.close();
			ps.close();
			cn = null;
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
	}
	
	public double getTotalSolicitud(int ID) {
		double total=0;
		Connection cn =null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		try {
			
			cn = BD.getConnection();
			ps = cn.prepareStatement("select getTotalSolicitud(?)");
			ps.setInt(1, ID);
			
			rs = ps.executeQuery();
			
			if(rs.next()) {
				total = rs.getDouble(1);
			}
			
			cn.close();
			ps.close();
			rs.close();
			cn=null;
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		return total;
	}
	
	public List<Carrito> getProductoRequest(int ID){
		List<Carrito> carritos = new ArrayList<>();
		Connection cn=null;
		CallableStatement cs=null;
		ResultSet rs=null;
		
		try {
			
			cn = BD.getConnection();
			cs = cn.prepareCall("{call getProductRequest(?)}");
			cs.setInt(1, ID);
			
			rs = cs.executeQuery();
			
			while(rs.next()) {
				Carrito carrito = new Carrito();
				carrito.setFK_S(rs.getInt("Solicitud"));
				carrito.setFK_P(rs.getInt("ID_P"));
				carrito.setNombre(rs.getString("Nombre"));
				carrito.setCantidad(rs.getInt("Cantidad"));
				carrito.setPrecioSu(rs.getDouble("Precio"));
				carrito.setTotal(rs.getDouble("Total"));
				
				carritos.add(carrito);
			}
			cn.close();
			cs.close();
			rs.close();
			cn=null;
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		return carritos;
	}
	
	public List<ViewRequest> getSolicitud(String estado){
		List<ViewRequest> solicitudes = new ArrayList<>();
		Connection cn=null;
		CallableStatement cs=null;
		ResultSet rs=null;
		System.out.println("Se pide: "+estado);
		try {
			
			cn = BD.getConnection();
			cs = cn.prepareCall("{call getRequest(?)}");
			cs.setString(1, estado);
			
			rs = cs.executeQuery();
			
			while(rs.next()) {
				ViewRequest vista = new ViewRequest();
				vista.setEstado(rs.getString("Estado"));
				vista.setID_Solicitud(rs.getInt("ID_Solicitud"));
				vista.setNick(rs.getString("Nick"));
				vista.setTotal(rs.getDouble("Total"));
				vista.setFecha_Inicio(rs.getDate("Fecha_Inicio"));
				vista.setCantidadProductos(rs.getInt("CantidadP"));
				vista.setID_User(rs.getInt("ID_User"));
				vista.setCompra(rs.getString("Compra"));
				solicitudes.add(vista);
			}
			cs.close();
			cn.close();
			rs.close();
			cn=null;
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		return solicitudes;
	}
	
	public void updateSolicitud(String estado,String remision,String compra,String pago,String envio) {
		Connection cn=null;
		PreparedStatement ps =null;
		
		try {
			
			cn = BD.getConnection();
			ps = cn.prepareStatement("select updateSolicitud(?,?,?,?,?)");
			
			ps.setString(1, estado);
			ps.setString(2, remision);
			ps.setString(3, compra);
			ps.setString(4, pago);
			ps.setString(5, envio);
			
			ps.execute();
			
			cn.close();
			ps.close();
			
			cn=null;
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
	}
	
	public void crearSolicitud() {
		Connection cn=null;
		PreparedStatement ps = null;
		
		
		try {
			
			cn =  BD.getConnection();
			ps = cn.prepareStatement("select createSolicitud()");
			
			ps.execute();
			
			cn.close();
			ps.close();
			
			cn=null;
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
	}
	
	public int getLastIDSolicitud() {
		int ID = 0;
		Connection cn=null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		
		try {
			cn=BD.getConnection();
			ps = cn.prepareStatement("select getLastSolicitudID()");
			
			rs = ps.executeQuery();
			
			if(rs.next()) {
				ID = rs.getInt(1);
			}
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		return ID;
	}
}
