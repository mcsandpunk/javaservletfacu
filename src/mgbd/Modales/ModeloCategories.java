package mgbd.Modales;

import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import mgbd.classes.Categories;

public class ModeloCategories {

	private DataSource BD;
	
	public ModeloCategories(DataSource origen) {
		this.BD = origen;
	}
	
	public List<Categories> getCategories(){
		
		List<Categories> categories = new ArrayList<>();
		Connection cn=null;
		CallableStatement cs=null;
		ResultSet rs=null;
		
		try {
			
			cn=BD.getConnection();
			cs=cn.prepareCall("{call getCategories()}");
			
			rs=cs.executeQuery();
			
			while(rs.next()) {
				Categories categoria = new Categories();
				
				categoria.setCatego(rs.getString("Nombre"));
				categoria.setEstado(rs.getBoolean("Estado"));
				categoria.setID(rs.getInt("ID_Categoria"));
				
				categories.add(categoria);
			}
			
			cn.close();
			cs.close();
			rs.close();
			cn=null;
			
		}catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
            	cn.close();
            	cs.close();
            	rs.close();
            	cn=null;
            } catch (Exception ex) {
            }
        }
		
		return categories;
	}
}
