package mgbd.Modales;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import mgbd.classes.Video;

public class ModeloVideo {

	DataSource BD;
	
	public ModeloVideo(DataSource origen) {
		this.BD = origen;
	}
	
	public void DeleteVideoProducto(int ID) {
		Connection cn=null;
		PreparedStatement ps=null;
		
		try {
			cn = BD.getConnection();
			ps = cn.prepareStatement("select deleteVideoFromProduct(?)");
			
			ps.setInt(1, ID);
			
			ps.execute();
			
			cn.close();
			ps.close();
			cn=null;
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
	}
	
	public List<Video> getAllVideoFromProducto(int ID){
		List<Video> videos= new ArrayList<>();
		Connection cn=null;
		CallableStatement cs =null;
		ResultSet rs=null;
		try {
			cn=BD.getConnection();
			cs = cn.prepareCall("{call getAllVideoFromProduct(?)}");
			
			cs.setInt(1, ID);
			
			rs = cs.executeQuery();
			
			while(rs.next()) {
				Video video = new Video();
				
				video.setiD(rs.getInt("ID_Video"));
				video.setDireccion(rs.getString("Localizacion"));
				
				videos.add(video);
			}
			cn.close();
			cs.close();
			rs.close();
			
			cn=null;
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		return videos;
	}
	
	public void AddVideo(int ID_Producto,String Localizacion) {
		Connection cn=null;
		CallableStatement cs = null;
		
		try {
			
			cn = BD.getConnection();
			
			cs = cn.prepareCall("{call addVideo(?,?)}");
			
			cs.setInt(1, ID_Producto);
			cs.setString(2, Localizacion);
			
			cs.execute();
			
			cn.close();
			cs.close();
			
			cn=null;
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
	}
}
