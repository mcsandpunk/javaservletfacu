package mgbd.Modales;

import java.sql.*;

import javax.sql.DataSource;

import mgbd.classes.Mailer;
import mgbd.classes.User;

public class ModeloUser {

	private DataSource BD;
	
	public ModeloUser(DataSource origen) {
		this.BD = origen;
	}
	
	public void updateUsuario(User usuario,String contra) {
		Connection cn=null;
		CallableStatement cs=null;
		
		try {
			cn = BD.getConnection();
			cs = cn.prepareCall("{ call updateUser(?,?,?,?,?,?,?,?)}");
			
			cs.setBlob(1, usuario.getImagen());
			cs.setString(2, usuario.getNombre());
			cs.setString(3, usuario.getApellido());
			cs.setString(4,usuario.getEmail());
			cs.setString(5, usuario.getNickname());
			cs.setString(6,contra);
			cs.setBoolean(7, usuario.isEstado());
			cs.setInt(8,usuario.getID());
			
			cs.execute();
			
			cn.close();
			cs.close();
			cn=null;
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
	}
	
	public User getUserImage(int ID) {
		User usuario=null;
		Connection cn=null;
		ResultSet rs=null;
		PreparedStatement ps = null;
		
		try {
			cn=BD.getConnection();
			ps = cn.prepareStatement("SELECT getImageUser(?)");
			
			ps.setInt(1, ID);
			
			rs=ps.executeQuery();
			
			if(rs.next()) {
				usuario=new User();
				usuario.setImagenByte(rs.getBytes(1));
			}
			
			cn.close();
			cn = null;
			ps.close();
			rs.close();
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		return usuario;
	}
	
	public User getUser(int ID) {
		User usuario=null;
		Connection cn = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			cn = BD.getConnection();
			cs = cn.prepareCall("{call getUserID(?)}");
			cs.setInt(1, ID);
			
			rs=cs.executeQuery();
			
			if(rs.next()) {
				usuario = new User();
				usuario.setID(rs.getInt("ID_Usuario"));
				usuario.setImagenByte(rs.getBytes("Imagen"));
				usuario.setNombre(rs.getString("Nombre"));
				usuario.setApellido(rs.getString("Apellido"));				
				usuario.setEmail(rs.getString("Email"));
				usuario.setNickname(rs.getString("Nickname"));
				usuario.setNivel(rs.getShort("Nivel"));
				usuario.setFecha_Creacion(rs.getDate("Fecha_Creacion"));
				usuario.setEstado(rs.getBoolean("Estado"));
			}
			
			cs.close();
			cn.close();
			rs.close();
			cn=null;
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		return usuario;
	}
	
	public String AddUser(User usuario,String contra) {
		String respuesta=null;
		Connection cn=null;
		CallableStatement cs=null;

		
		try {
			cn=BD.getConnection();
			cs=cn.prepareCall("{call AddUsuario(?,?,?,?,?,?,?,?,?)}");
			cs.setBlob(1, usuario.getImagen());
			cs.setString(2, usuario.getNombre());
			cs.setString(3, usuario.getApellido());
			cs.setString(4, usuario.getEmail());
			cs.setString(5, usuario.getNickname());
			cs.setString(6, contra);
			cs.setShort(7, usuario.getNivel());			
			cs.setDate(8,null );
			cs.setBoolean(9, usuario.isEstado());
			
			cs.execute();
			
			cs.close();
			cn.close();
			cn=null;
			
			respuesta = "Nice";
			//Mailer.send(usuario.getEmail());
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				respuesta=ex.getMessage();
				String sub = respuesta.substring(respuesta.length()-6,respuesta.length()-1);
				System.out.println(sub);
				if(sub.equals("Email")) {
					respuesta = "SameEmail";
				}
				else {
					respuesta = "SameNickname";
				}
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		return respuesta;
	}
	
	public User CheckUser(String email,String contra) {
		Connection cn=null;
		CallableStatement cs=null;
		ResultSet rs =null;
		
		User usuario = null;
		
		try {
			cn=BD.getConnection();
			
			cs=cn.prepareCall("{Call getUser(?)}");
			cs.setString(1, email);
			
			rs=cs.executeQuery();
			
			if(rs.next()) {
				
				if(contra.equals(rs.getString("Contra"))) {
					usuario = new User();
					usuario.setID(rs.getInt("ID_Usuario"));
					usuario.setImagenByte(rs.getBytes("Imagen"));
					usuario.setNombre(rs.getString("Nombre"));
					usuario.setApellido(rs.getString("Apellido"));				
					usuario.setEmail(rs.getString("Email"));
					usuario.setNickname(rs.getString("Nickname"));
					usuario.setNivel(rs.getShort("Nivel"));
					usuario.setFecha_Creacion(rs.getDate("Fecha_Creacion"));
					
					cn.close();
					cn=null;
					cs.close();
					rs.close();
					
				}
				cn.close();
				cn=null;
				cs.close();
				rs.close();
			}
			else {
				cn.close();
				cn=null;
				cs.close();
				rs.close();
			}
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("ERROR SQL CHECK USER");
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
			System.out.println("ERROR CHECK USER");
		}finally {
			try {
				cs.close();
				cn.close();
				cn=null;
				rs.close();
			}catch(Exception ex) {
				System.out.println("ERROR FINALLY CHECK USER");
			}
		}
		return usuario;
	
	}
}
