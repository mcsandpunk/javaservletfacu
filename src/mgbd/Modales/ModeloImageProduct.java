package mgbd.Modales;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import mgbd.classes.ImageProduct;

public class ModeloImageProduct {

	DataSource BD;
	
	public ModeloImageProduct(DataSource origen){
		this.BD = origen;
	}
	
	public int stillImages(int ID_Producto) {
		Connection cn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int cantidad=0;
		try {
			
			cn=BD.getConnection();
			
			ps = cn.prepareStatement("select getCountImagesProduct(?)");
			
			ps.setInt(1, ID_Producto);
			
			rs = ps.executeQuery();
		
			if(rs.next()) {
				cantidad = rs.getInt(1);
			}
		
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		
	return cantidad;
	}
	
	public void DeleteImageFromProduct(int ID_Imagen) {
		Connection cn=null;
		PreparedStatement ps = null;
		
		try {
			cn=BD.getConnection();
			
			ps = cn.prepareStatement("select deleteImageProduct(?)");
			
			ps.setInt(1, ID_Imagen);
			
			ps.execute();
			
			cn.close();
			ps.close();
			cn=null;
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				
				cn=null;
			}catch(Exception ex) {
				
			}
		}
	}
	
	public List<ImageProduct> getAllImagesProduct(int ID_Product){
		List<ImageProduct> imagenes = new ArrayList<>();
		
		Connection cn =null;
		CallableStatement cs=null;
		ResultSet rs=null;
		
		try {
			cn = BD.getConnection();
			cs = cn.prepareCall("{call getImagesProducts(?)}");
			
			cs.setInt(1, ID_Product);
			
			rs = cs.executeQuery();
			
			while(rs.next()) {
				ImageProduct imagen = new ImageProduct();
				imagen.setID(rs.getInt("ID_Imagen"));
				imagen.setLocalizacion(rs.getString("Localizacion"));
				
				imagenes.add(imagen);
			}
			
			cn.close();
			rs.close();
			cs.close();
			
			cn=null;
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		return imagenes;
	}
	
	public String getProductImage(int ID_Product) {
		String direccion = null;
		
		Connection cn=null;
		PreparedStatement ps = null;
		ResultSet rs= null;
		try {
			
			cn = BD.getConnection();
			
			ps = cn.prepareStatement("select getProductImage(?)");
			
			ps.setInt(1, ID_Product);
			
			rs = ps.executeQuery();
			
			if(rs.next()) {
				return rs.getString(1);
			}
			
			cn.close();
			ps.close();
			rs.close();
			cn=null;
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				ps.close();
				cn.close();
				rs.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
		return direccion;
	}
	
	public void addImage(String ruta,int ID_Producto) {
		Connection cn = null;
		CallableStatement cs = null;
		ResultSet rs= null;
		
		
		try {
			
			cn = BD.getConnection();
			cs = cn.prepareCall("{call addImagenProduct(?,?)}");
			
			cs.setString(1, ruta);
			cs.setInt(2, ID_Producto);
			
			cs.execute();
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			if(ex.getErrorCode()==1062) {
				System.out.println("Duplicado");
			}
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}finally {
			try {
				cs.close();
				cn.close();
				cn=null;
			}catch(Exception ex) {
				
			}
		}
		
	}
	
}
