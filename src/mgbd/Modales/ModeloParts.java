package mgbd.Modales;

import java.sql.*;
import java.util.*;

import javax.sql.DataSource;

import mgbd.classes.Parts;


public class ModeloParts {

	private DataSource BD;
	
	public ModeloParts(DataSource origen) {
		this.BD = origen;
	}
	
	public List<Parts> getParts(){
		List<Parts> ListaParts = new ArrayList<>();
		
		Connection miConexion = null;
		
		ResultSet miResultSet = null;
		
		//Establecer Conexion
		try{
			miConexion=BD.getConnection();
		
		//Sentencia SQL
		String instru = "Call getAllParts()";
		
		CallableStatement stmt=miConexion.prepareCall(instru);
		
		//miStatement = miConexion.createStatement();
		//Ejecutar SQL
		
		//miResultSet = miStatement.executeQuery(instru);
		miResultSet = stmt.executeQuery();
		//Recorrer ResultSet
		
			while(miResultSet.next()) {
				int ID = miResultSet.getInt("ID_Part");
				String nombre = miResultSet.getString("Part");
				int Estado = miResultSet.getInt("Estado");
				
				
				Parts tempParts = new Parts(ID,nombre,Estado);
				
				ListaParts.add(tempParts);
			}
			miConexion.close();
			miConexion = null;
		}catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
            	miResultSet.close();            	
            	miConexion.close();
            	miConexion=null;
            } catch (Exception ex) {
            }
        }
		
		return ListaParts;
	}
}
